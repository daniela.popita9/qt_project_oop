#pragma once
class UnknownEnumCodeException
{
public:
	UnknownEnumCodeException();

	const char* what() const throw() 
	{
		return "No match for the given code\n";
	}

	~UnknownEnumCodeException();

};

