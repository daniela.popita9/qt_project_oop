#include <exception>
#include <assert.h>
#include <iostream>
#include "test_classes.h"
#include "Jewelry.h"
#include "Necklace.h"
#include "Ring.h"
#include "Earrings.h"
#include "Repository.h"
#include "Controller.h"


using namespace std;


void testJewelryCreate()
{
	cout << "Test create for jewelry class\n";
	Jewelry jewelry(GOLD, 234.5, 23, true, "Everyday Diamond");
	
	assert(jewelry.getId() == "J1");
	assert(jewelry.getMaterial() == GOLD);
	assert(jewelry.getPrice() == 234.5);
	assert(jewelry.getWeight() == 23);
	assert(jewelry.getGemstone());
	assert(jewelry.getCollection() == "Everyday Diamond");

	Jewelry::getToNextId();
	Jewelry id_test_jewelry;
	assert(id_test_jewelry.getId() == "J2");

	Jewelry copy_jewelry(jewelry);
	assert(copy_jewelry.getId() == "J1");
	assert(copy_jewelry.getMaterial() == GOLD);
	assert(copy_jewelry.getPrice() == 234.5);
	assert(copy_jewelry.getWeight() == 23);
	assert(copy_jewelry.getGemstone());
	assert(copy_jewelry.getCollection() == "Everyday Diamond");

	Jewelry assignment_jewelry = jewelry;
	assert(assignment_jewelry.getId() == "J1");
	assert(assignment_jewelry.getMaterial() == GOLD);
	assert(assignment_jewelry.getPrice() == 234.5);
	assert(assignment_jewelry.getWeight() == 23);
	assert(assignment_jewelry.getGemstone());
	assert(assignment_jewelry.getCollection() == "Everyday Diamond");

	jewelry.setCollection("Paloma Picasso");
	assert(jewelry.getCollection() == "Paloma Picasso");

	jewelry.setGemstone(false);
	assert(jewelry.getGemstone()==false);

	jewelry.setMaterial(SILVER);
	assert(jewelry.getMaterial() == SILVER);

	jewelry.setPrice(5000);
	assert(jewelry.getPrice() == 5000);
	try {
		jewelry.setPrice
		(-1);
		assert(false);
	}
	catch (NegativeNumberException&) {
		assert(true);
	}

	jewelry.setWeight(12);
	try {
		jewelry.setWeight(-1);
		assert(false);
	}
	catch (NegativeNumberException&) {
		assert(true);
	}
	try {
		jewelry.setWeight(2000);
		assert(false);
	}
	catch (TooHeavyWeightException&) {
		assert(true);
	}

	assert(jewelry.getWeight() == 12);
	jewelry.updateJewelry(1,"Update collection");
	assert(jewelry.getPrice() == 1);
	assert(jewelry.getCollection() == "Update collection");
	try {
		jewelry.updateJewelry(-1, "Update collection");
		assert(false);
	}
	catch (NegativeNumberException&) {
		assert(true);
	}

	Jewelry* cast_jewelry = new Jewelry();
	assert(dynamic_cast<Necklace*>(cast_jewelry) == nullptr);
	assert(dynamic_cast<Ring*>(cast_jewelry) == nullptr);
	assert(dynamic_cast<Earrings*>(cast_jewelry) == nullptr);

	delete cast_jewelry;

	cout << "Test create for Material enum \n";

	assert(MapMaterialToString(jewelry.getMaterial()) == "silver");
	assert(MapMaterialToString(GOLD) == "gold");
	assert(MapMaterialToString(PLATINUM) == "platinum");
	assert(MapToMaterial(1)==GOLD);
	assert(MapToMaterial(2) == SILVER);
	assert(MapToMaterial(3) == PLATINUM);
}

void testNecklaceCreate()
{
	cout << "Test create for necklace class\n";
	Necklace necklace(PLATINUM, 2600, 17, false, "Crescent Crown", 46);
	assert(necklace.getId() == "J2N1");
	assert(necklace.getMaterial() == PLATINUM);
	assert(necklace.getPrice() == 2600);
	assert(necklace.getWeight() == 17);
	assert(necklace.getGemstone()==false);
	assert(necklace.getCollection() == "Crescent Crown");
	assert(necklace.getLength()== 46);

	Necklace::GetToNextNecklaceId();
	Necklace id_test_necklace;
	assert(id_test_necklace.getId() == "J2N2");

	Necklace copy_necklace(necklace);
	assert(copy_necklace.getId() == "J2N1");
	assert(copy_necklace.getMaterial() == PLATINUM);
	assert(copy_necklace.getPrice() == 2600);
	assert(copy_necklace.getWeight() == 17);
	assert(copy_necklace.getGemstone() == false);
	assert(copy_necklace.getCollection() == "Crescent Crown");
	assert(copy_necklace.getLength() == 46);

	necklace.setLength(5.8);
	
	Necklace assignment_necklace=necklace;
	assert(assignment_necklace.getId() == "J2N1");
	assert(assignment_necklace.getMaterial() == PLATINUM);
	assert(assignment_necklace.getPrice() == 2600);
	assert(assignment_necklace.getWeight() == 17);
	assert(assignment_necklace.getGemstone() == false);
	assert(assignment_necklace.getCollection() == "Crescent Crown");
	//assert(assignment_necklace.getLength() == 5.8);

	Jewelry* cast_necklace= new Necklace();
	assert(dynamic_cast<Necklace*>(cast_necklace)!= nullptr);
	assert(dynamic_cast<Jewelry*>(cast_necklace) != nullptr);

	assert(dynamic_cast<Ring*>(cast_necklace) == nullptr);
	assert(dynamic_cast<Earrings*>(cast_necklace) == nullptr);

	delete cast_necklace;
}

void testRingCreate()
{
	cout << "Test create for ring class\n";
	
	Jewelry::getToNextId();

	Ring ring( SILVER, 3800, 17, true, "Sonoma Mist", 24);
	assert(ring.getId() == "J3R1");
	assert(ring.getMaterial() == SILVER);
	assert(ring.getPrice() == 3800);
	assert(ring.getWeight() == 17);
	assert(ring.getGemstone() == true);
	assert(ring.getCollection() == "Sonoma Mist");
	assert(ring.getSize() == 24);

	Ring::GetToNextRingId();
	Ring id_test_ring;
	assert(id_test_ring.getId() == "J3R2");

	Ring copy_ring(ring);
	assert(copy_ring.getId() == "J3R1");
	assert(copy_ring.getMaterial() == SILVER);
	assert(copy_ring.getPrice() == 3800);
	assert(copy_ring.getWeight() == 17);
	assert(copy_ring.getGemstone() == true);
	assert(copy_ring.getCollection() == "Sonoma Mist");
	assert(copy_ring.getSize() == 24);

	ring.setSize(22);

	Ring assignment_ring=ring;

	assert(assignment_ring.getId() == "J3R1");
	assert(assignment_ring.getMaterial() == SILVER);
	assert(assignment_ring.getPrice() == 3800);
	assert(assignment_ring.getWeight() == 17);
	assert(assignment_ring.getGemstone() == true);
	assert(assignment_ring.getCollection() == "Sonoma Mist");
	assert(assignment_ring.getSize() == 22);

	Jewelry* cast_ring = new Ring();
	assert(dynamic_cast<Ring*>(cast_ring) != nullptr);
	assert(dynamic_cast<Jewelry*>(cast_ring) != nullptr);
	
	assert(dynamic_cast<Necklace*>(cast_ring) == nullptr);
	assert(dynamic_cast<Earrings*>(cast_ring) == nullptr);

	delete cast_ring;

}

void testEarringsCreate()
{
	cout << "Test create for earring class\n";

	Jewelry::getToNextId();

	Earrings earrings(GOLD, 1200, 8, true, "Perfect Cut", DROPS, false);
	assert(earrings.getId() == "J4E1");
	assert(earrings.getMaterial() == GOLD);
	assert(earrings.getPrice() == 1200);
	assert(earrings.getWeight() == 8);
	assert(earrings.getGemstone() == true);
	assert(earrings.getCollection() == "Perfect Cut");
	assert(earrings.getEaringType() == DROPS);
	assert(earrings.getIsPiercing() == false);

	Earrings::GetToNextEarringId();
	Earrings id_test_earring;
	assert(id_test_earring.getId() == "J4E2");

	Earrings copy_earrings(earrings);
	assert(copy_earrings.getId() == "J4E1");
	assert(copy_earrings.getMaterial() == GOLD);
	assert(copy_earrings.getPrice() == 1200);
	assert(copy_earrings.getWeight() == 8);
	assert(copy_earrings.getGemstone() == true);
	assert(copy_earrings.getCollection() == "Perfect Cut");
	assert(copy_earrings.getEaringType() == DROPS);
	assert(copy_earrings.getIsPiercing() == false);

	earrings.setIsPiercing(true);
	earrings.setEaringType(STUDS);

	Earrings assignment_earrings= earrings;
	assert(assignment_earrings.getId() == "J4E1");
	assert(assignment_earrings.getMaterial() == GOLD);
	assert(assignment_earrings.getPrice() == 1200);
	assert(assignment_earrings.getWeight() == 8);
	assert(assignment_earrings.getGemstone() == true);
	assert(assignment_earrings.getCollection() == "Perfect Cut");
	assert(assignment_earrings.getEaringType() == STUDS);
	assert(assignment_earrings.getIsPiercing() == true);

	Jewelry* cast_earring = new Earrings();
	assert(dynamic_cast<Earrings*>(cast_earring) != nullptr);
	assert(dynamic_cast<Jewelry*>(cast_earring) != nullptr);

	assert(dynamic_cast<Necklace*>(cast_earring) == nullptr);
	assert(dynamic_cast<Ring*>(cast_earring) == nullptr);

	cout << "Test create for EarringType enum \n";
	assert(MapEaringTypeToString(STUDS) == "studs");
	assert(MapEaringTypeToString(HOOPS) == "hoops");
	assert(MapEaringTypeToString(DROPS) == "drops");
	assert(MapToEaringType(1) == STUDS);
	assert(MapToEaringType(2) == HOOPS);
	assert(MapToEaringType(3) == DROPS);
	
	delete cast_earring;
}

void testRepositoryAdd()
{
	cout << "Test create for the repository\n";

	Jewelry* jewelry= new Jewelry ( GOLD, 234.5, 23, true, "Everyday Diamond");
	Jewelry* necklace= new Necklace( PLATINUM, 2600, 17, false, "Crescent Crown", 46);
	Jewelry* ring= new Ring( SILVER, 3800, 17, true, "Sonoma Mist", 24);
	Jewelry* earrings = new Earrings( GOLD, 1200, 8, true, "Perfect Cut", DROPS, false);

	Repository test_repo;

	test_repo.add(jewelry);
	test_repo.add(necklace);
	test_repo.add(ring);
	test_repo.add(earrings);

	assert(test_repo.getRepoSize() == 4);
}

void testController()
{
	cout << "Test create for the controller\n";

	Controller test_controller;

	test_controller.addNecklace(SILVER, 21200, 22, true, "Sonoma Mist", 28);
	test_controller.addNecklace(PLATINUM, 2600, 17, false, "Perfect Cut", 46);
	test_controller.addNecklace(GOLD, 1350, 34, true, "Everyday Diamond", 52);

	test_controller.addRing(SILVER, 3800, 17, false, "Sonoma Mist", 24);
	test_controller.addRing(GOLD, 4500, 25, true, "Perfect Cut", 22);
	test_controller.addRing(PLATINUM, 3800, 19, false, "Everyday Diamond", 28);


	test_controller.addEarrings(GOLD, 1200, 8, false, "Perfect Cut", DROPS, true);
	test_controller.addEarrings(SILVER, 2300, 10, true, "Sonoma Mist", HOOPS, false);
	test_controller.addEarrings(PLATINUM, 600, 14, true, "Everyday Diamond", STUDS, false);


	//cout<<test_controller;


	//controller jewelry operations testing;
	std::vector<Jewelry*> jewelry_repo= test_controller.getAllJewelryMadeOfMaterial(GOLD);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getMaterial() == GOLD);
	}
	
	jewelry_repo = test_controller.getAllJewelryMadeOfMaterial(SILVER);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getMaterial() == SILVER);
	}

	jewelry_repo = test_controller.getAllJewelryMadeOfMaterial(PLATINUM);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getMaterial() == PLATINUM);
	}
	
	jewelry_repo = test_controller.getAllJewelryWithGemstone();
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getGemstone());
	}

	jewelry_repo = test_controller.getAllJewelryWithoutGemstone();
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getGemstone()==false);
	}

	jewelry_repo = test_controller.getAllJewelryWithPriceLessThan(5000);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getPrice() <= 5000);
	}

	jewelry_repo = test_controller.getAllJewelryWithPriceEqualTo(5000);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getPrice() == 4500);
	}

	jewelry_repo = test_controller.getAllJewelryFromCollection("Perfect Cut");	
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getCollection() == "Perfect Cut");
	}
	
	jewelry_repo = test_controller.getAllJewelryFromCollection("Sonoma Mist");
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getCollection() == "Sonoma Mist");
	}

	jewelry_repo = test_controller.getAllJewelryFromCollection("Everyday Diamond");
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(jewelry->getCollection() == "Everyday Diamond");
	}

	jewelry_repo = test_controller.getAllNecklaces();
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Necklace*>(jewelry));
	}

	jewelry_repo = test_controller.getAllNecklacesOfLengthLessThan(55);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Necklace*>(jewelry)->getLength() <= 55);
	}

	jewelry_repo = test_controller.getAllNecklacesOfLengthGreaterThan(20);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Necklace*>(jewelry)->getLength() >= 20);
	}

	jewelry_repo = test_controller.getAllNecklacesOfLengthEqualTo(45);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Necklace*>(jewelry)->getLength() == 45);
	}
	
	
	//controller ring operations testing;
	
	jewelry_repo = test_controller.getAllRings();
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Ring*>(jewelry));
	}

	jewelry_repo = test_controller.getAllRingsOfSizeEqualTo(24);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Ring*>(jewelry)->getSize()==24);
	}

	jewelry_repo = test_controller.getAllRingsOfSizeGreaterThan(20);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Ring*>(jewelry)->getSize() >= 20);
	}

	jewelry_repo = test_controller.getAllRingsOfSizeLessThan(27);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Ring*>(jewelry)->getSize() <= 27);
	}


	//controller earring operations testing;
	jewelry_repo = test_controller.getAllEarrings();
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Earrings*>(jewelry));
	}

	jewelry_repo = test_controller.getAllPiercings();
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Earrings*>(jewelry)->getIsPiercing());
	}

	jewelry_repo = test_controller.getAllPairsOfEarrings();
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Earrings*>(jewelry)->getIsPiercing()==false);
	}

	jewelry_repo = test_controller.getAllEarringsOfType(DROPS);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Earrings*>(jewelry)->getEaringType() == DROPS);
	}

	jewelry_repo = test_controller.getAllEarringsOfType(HOOPS);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Earrings*>(jewelry)->getEaringType() == HOOPS);
	}

	jewelry_repo = test_controller.getAllEarringsOfType(STUDS);
	for (Jewelry* jewelry : jewelry_repo)
	{
		assert(dynamic_cast<Earrings*>(jewelry)->getEaringType() == STUDS);
	}
	
	assert(test_controller.RemoveById("J6R3") == true);
	assert(test_controller.RemoveById("J9E3") == true);

	assert(test_controller.RemoveById("J6R3") == false);
	assert(test_controller.RemoveById("J9E3") == false);
	assert(test_controller.GetRepoSize() == 7);

	assert(test_controller.PerformUndo() == 1);
	assert(test_controller.GetRepoSize() == 8);
	assert(test_controller.PerformRedo() == 1);
	assert(test_controller.GetRepoSize() == 7);
	

	assert(test_controller.PerformUndo() == 1);
	assert(test_controller.GetRepoSize() == 8);

	assert(test_controller.PerformUndo() == 1);
	assert(test_controller.GetRepoSize() == 9);//

	assert(test_controller.UpdateById("J2N2", 23, "DD")== true);
	assert(test_controller.UpdateById("1N1", 2, "fdscxd")== 0);
	assert(test_controller.UpdateById("1N1", 23, "DD")== false);

	//delete */
}

void testClasses()
{
	testJewelryCreate();
	testNecklaceCreate();
	testRingCreate();
	testEarringsCreate();
	testRepositoryAdd();
	testController();
}
