#include "Jewelry.h"
#include <exception>
#include "UnknownEnumCodeException.h"

Material MapToMaterial(int materialCode) 
{
	switch (materialCode)
	{
	case 1:
		return GOLD;
		break;
	case 2:
		return SILVER;
		break;
	case 3:
		return PLATINUM;
		break;
	default:
		throw UnknownEnumCodeException();
		break;
	}

}

std::string MapMaterialToString(Material material) 
{
	switch (material)
	{
	case GOLD:
		return "gold";
		break;
	case SILVER:
		return "silver";
		break;
	case PLATINUM:
		return "platinum";
		break;
	default:
		throw UnknownEnumCodeException();
		break;
	}
}

int Jewelry::Nextid = 1;

Jewelry::Jewelry()
{
	this->id.append("J" + std::to_string(Nextid));
}

Jewelry::Jewelry( Material material, double price, float weight, bool gemstone, std::string collection)
{
	this->material = material;
	if (price <= 0)
		throw NegativeNumberException();
	this->price = price;
	
	if(weight<=0)
		throw NegativeNumberException();
	if (weight > 500)
		throw TooHeavyWeightException();

	this->weight = weight;

	this->gemstone = gemstone;
	this->collection = collection;
	this->id.append("J" + std::to_string(Nextid));
}

Jewelry::Jewelry(const Jewelry& other)
{
	this->id = other.id;
	this->material = other.material;
	this->price = other.price;
	this->weight = other.weight;
	this->gemstone = other.gemstone;
	this->collection = other.collection;
}

Jewelry& Jewelry::operator=(const Jewelry& other)
{
	this->id = other.id;
	this->material = other.material;
	this->price = other.price;
	this->weight = other.weight;
	this->gemstone = other.gemstone;
	this->collection = other.collection;
	return *this;
}

std::string Jewelry::DisplayJewelry () const
{
	std::string stringVersion;
	stringVersion.append("This is a jewelry of id: " + this->id + ". It is made from: " + MapMaterialToString(this->material));

	if (this->gemstone)
		stringVersion.append(", it has a gemstone");

	stringVersion.append(", it weights " + std::to_string(this->weight) + " grams and it costs " + std::to_string(this->price) + "$. " + "This jewelry is part of " + this->collection + " collection.\n");

	return stringVersion;
}

void Jewelry::updateJewelry( double new_price, std::string new_collection)
{
	if (new_price <= 0)
		throw NegativeNumberException();
	this->price = new_price;
	this->collection = new_collection;
}

std::ostream& operator<<(std::ostream& out, const Jewelry& jewelry)
{
	out << "Jewelry of id: " << std::setw(6) << std::right << jewelry.id << ". It is made from: " << std::setw(9) << MapMaterialToString(jewelry.material);
	if (jewelry.getGemstone())
		out << std::setw(27) << "it has a gemstone";
	else {
		out << std::setw(27) << "it doesn't have a gemstone";
	}
	out << ", it weights " << std::setw(3) << std::setprecision(2) << jewelry.weight << " grams, it costs " << std::setw(8) << std::setprecision(8) << std::right << jewelry.price << "$, " << "and is part of " << std::setw(20) << jewelry.collection << " collection.\n" << std::endl;

	return out;
}

void Jewelry::display(std::ostream& out) const
{
	out << "Jewelry of id: " << std::setw(6) << std::right << this->id << ". It is made from: " << std::setw(9) << MapMaterialToString(this->material);
	if (this->getGemstone())
		out << std::setw(27) << "it has a gemstone";
	else {
		out << std::setw(27) << "it doesn't have a gemstone";
	}
	out << ", it weights " << std::setw(3) << std::setprecision(2) << this->weight << " grams, it costs " << std::setw(8) << std::setprecision(8) << std::right << this->price << "$, " << "and is part of " << std::setw(20) << this->collection << " collection.\n" << std::endl;
}

Jewelry::~Jewelry()
{
	std::cout << "Destructed" << std::endl;
}