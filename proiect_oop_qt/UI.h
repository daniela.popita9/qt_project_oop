#pragma once
#include "Controller.h"

#include<iostream>
class UI
{
/*
* private attributes and methoods
*/
private:
	/*
	*ctrl- object of type controller
	*/
	Controller ctrl;
	/*
	* showMenu - prints the menu on the screen
	*/
	void showMenu() const;

	/*printJewelryVector - prints a vector of objects of type Jewelry*(pointers to jewelry objects);
	* Parameters: std::vector<Jewelry*> JewelryVector -  a vector of objects of type Jewelry*(pointers to jewelry objects);
	*/
	void printJewelryVector(std::vector<Jewelry*> JewelryVector) const;

/*public methoods*/
public:
	
	/*
	* Constructor for the UI class:
	*/
	UI();
	/*
	* function for starting the aplication
	*/
	void run();
	
	int run_qt(int i, char** pString);
	/*
	* destructor for the UI class:
	*/
	~UI();

};
/*
* Validator for the material code of type int
* Throws: - UnknownEnumCodeException() if the code is not correct;
*/
int materialcode_validator();

/*
* Validator for the weight of type float
* Throws: -NegativeNumberException() if the weigth provided by the user is a negative number
*		  -TooHeavyWeightException() if the weigth is greater than 500
*/
float weight_validator();

/*
* Validator for the price of type double
* Throws: -NegativeNumberException() if the weigth provided by the user is a negative number
*/
double price_validator();

/*
* Validator for the gemstone of type bool
*/
bool gemstone_validator();

/*
* Validator for the earringtypecode code of type int
* Throws: - UnknownEnumCodeException() if the code is not correct;
*/
int earringtypecode_validator();

/*
* Validator for the ispiercing of type bool
*/
bool ispiercing_validator();

/*
* Validator for the size code of type float
* Throws: -NegativeNumberException() if the size provided by the user is a negative number
*/
float size_validator();

/*
* Validator for the length code of type float
* Throws: -NegativeNumberException() if the length provided by the user is a negative number
*/
float length_validator();


/*
* Validator for the collection of type std::string
*/
std::string collection_vaidator();

/*
* Validator for the id of type std::string
*/
std::string id_validator();