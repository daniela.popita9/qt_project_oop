/********************************************************************************
** Form generated from reading UI file 'proiect_oop_qt.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROIECT_OOP_QT_H
#define UI_PROIECT_OOP_QT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_proiect_oop_qtClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *proiect_oop_qtClass)
    {
        if (proiect_oop_qtClass->objectName().isEmpty())
            proiect_oop_qtClass->setObjectName(QString::fromUtf8("proiect_oop_qtClass"));
        proiect_oop_qtClass->resize(600, 400);
        menuBar = new QMenuBar(proiect_oop_qtClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        proiect_oop_qtClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(proiect_oop_qtClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        proiect_oop_qtClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(proiect_oop_qtClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        proiect_oop_qtClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(proiect_oop_qtClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        proiect_oop_qtClass->setStatusBar(statusBar);

        retranslateUi(proiect_oop_qtClass);

        QMetaObject::connectSlotsByName(proiect_oop_qtClass);
    } // setupUi

    void retranslateUi(QMainWindow *proiect_oop_qtClass)
    {
        proiect_oop_qtClass->setWindowTitle(QCoreApplication::translate("proiect_oop_qtClass", "proiect_oop_qt", nullptr));
    } // retranslateUi

};

namespace Ui {
    class proiect_oop_qtClass: public Ui_proiect_oop_qtClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROIECT_OOP_QT_H
