#pragma once
#include "Repository.h"
#include "UnknownEnumCodeException.h"

class Controller
{
/*private attributes*/
private :
	
	/*Repository repository- object of type Repository;*/
	Repository repository;

/*public methoods*/
public:

	/*	
	* Constructors for the controller class:
	* Default constructor with no parameters
	*/
	Controller();

	/*getter for the repository size
	* Returns an integer
	*/

	inline int GetRepoSize() { return this->repository.getRepoSize(); };

	/*
	* setter for the repository:
	* Params: Repository new_repository- an object of type repository;
	*/
	inline void SetControllerRepository(Repository new_repository) { this->repository=new_repository; };
	inline Repository GetRepository() { return this->repository; };


	/*
	* adds an earring/ a pair of earrings to the repository of the controller
	* Params -Material- material- the material the necklace is made from {GOLD, SILVER or PLATINUM}
	*			- double - price- the price of the earrings;
	*			- float  - weight- the weight of the earrings;
	*			- bool - gemstone -true if the earrings has a gemstone and false otherwise;
	*			- std:: string -collection- the collection the earrings is part from;
	*			- EaringType type - the type of the earrings (STUDS, HOOPS or DROPS);
	*			- bool- is piercing- false if the earrings is a pair of earings, true if it is a piercing
	*/
	void addEarrings( Material material, double price, float weight, bool gemstone, std::string collection, EaringType type, bool is_piercing);
	


	/*
	* adds a necklace to the repository of the controller
	* Params -Material- material- the material the necklace is made from {GOLD, SILVER or PLATINUM}
	*			- double - price- the price of the necklace;
	*			- float  - weight- the weight of the necklace;
	*			- bool - gemstone -true if the necklace has a gemstone and false otherwise;
	*			- std:: string -collection- the collection the necklace is part from;
	*			- float- length - the length of the necklace
	*/
	void addNecklace( Material material, double price, float weight, bool gemstone, std::string collection, float length);



	/*
	* adds a ring to the repository of the controller
	* Params -Material- material- the material the ring is made from {GOLD, SILVER or PLATINUM}
	*			- double - price- the price of the ring;
	*			- float  - weight- the weight of the ring;
	*			- bool - gemstone -true if the ring has a gemstone and false otherwise;
	*			- std:: string -collection- the collection the ring is part from;
	*			- float- length - the length of the ring
	*/
	void addRing(Material material, double price, float weight, bool gemstone, std::string collection, float size);



	/* 
	* getAllJewelry - returns a vector of all the jewelry from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelry() const;

	
	/* 
	* getAllJewelryMadeOfMaterial - returns a vector of all the objects that have a certain material from the repository of the controller
	* Params:- Material material - the material the jewelries need to have(GOLD, SILVER or PLATINUM)
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelryMadeOfMaterial(Material material) const;


	/*
	* getAllJewelryWithGemstone - returns a vector of all the objects that have gemstone from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelryWithGemstone() const;


	/*
	* getAllJewelryWithoutGemstone - returns a vector of all the objects that  don't have a gemstone from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelryWithoutGemstone() const;

	/*
	*  getAllJewelryWithPriceLessThan - returns a vector of all the objects that have a price less than a given price from the repository of the controller
	* Params:- double price - the price you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelryWithPriceLessThan(double price) const;


	/* 
	* getAllJewelryWithPriceGreaterThan - returns a vector of all the objects that have a price  greater than a given price from the repository of the controller
	* Params:- double price - the price you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelryWithPriceGreaterThan(double price) const;


	/*
	* getAllJewelryWithPriceEqualTo - returns a vector of all the objects that have a price equal to a given price from the repository of the controller
	* Params:- double price - the price you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelryWithPriceEqualTo(double price) const;


	/* 
	* getAllJewelryFromCollection - returns a vector of all the objects that are from the given collection from the repository of the controller
	* Params:- std::string collection - the collection from which you want the jewelry
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllJewelryFromCollection (std::string collection) const;



	/* 
	* getAllNecklaces - returns a vector of all the necklaces from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllNecklaces() const;


	/*
	* getAllNecklacesOfLengthLessThan - returns a vector of all the necklaces that have a length less than a given length from the repository of the controller
	* Params:- float length - the length you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllNecklacesOfLengthLessThan(float length) const;


	/*
	* getAllNecklacesOfLengthGreaterThan - returns a vector of all the necklaces that have a length greater than a given length from the repository of the controller
	* Params:- float length - the length you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllNecklacesOfLengthGreaterThan(float length) const;


	/*
	* get_all_necklaces_of_length - returns a vector of all the necklaces that have a length equal to a given length from the repository of the controller
	* Params:- float length - the length you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllNecklacesOfLengthEqualTo(float length) const;



	/* 
	* getAllRings - returns a vector of all the rings from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllRings() const;


	/* 
	* getAllRingsOfSizeLessThan - returns a vector of all the rings that have a size less than a given size from the repository of the controller
	* Pars:- float size - the size you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllRingsOfSizeLessThan(float size) const;


	/* 
	* getAllRingsOfSizeGreaterThan - returns a vector of all the rings that have a size greater than a given size from the repository of the controller
	* Params:- float size - the size you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllRingsOfSizeGreaterThan(float size) const;


	/* 
	* getAllRingsOfSizeEqualTo - returns a vector of all the rings that have a size equal to a given size from the repository of the controller
	* Params:- float size - the size you want to comapre with
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllRingsOfSizeEqualTo(float size) const;



	/* 
	* getAllEarrings - returns a vector of all the earrings from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*> getAllEarrings() const;


	/* 
	* getAllEarringsOfType - rreturns a vector of all the earrings that have a certain type from the repository of the controller
	* Params:- EaringType earringtype - the type the earrings need to have(STUDS, HOOPS or DROPS);
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllEarringsOfType(EaringType earringtype) const;


	/* 
	* getAllPairsOfEarrings - returns a vector of all the objects that aren't piercings from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllPairsOfEarrings() const;


	/* 
	* getAllPiercings - returns a vector of all the objects that are piercings from the repository of the controller
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>getAllPiercings() const;



	/*
	* Overload for the ostream operator <<
	* Params: - std::ostream& out- where you want to display the strig version of the controller,
	*		  - Controller& controller - the controller you want to display;
	*/
	friend std::ostream& operator<<(std::ostream& out, const Controller& controller);


	/*
	* RemoveById -  removes a jewelery by the given id from the repository
	* Params: std::string removeId - the id of the jewelry you want to remove
	* Returns true if the jewelry was removed, false otherwise
	*/
	bool RemoveById(std::string removeId);

	/*
	* updates a jewelry by id
	* Params: - std::string updateId - the id of the jewelry that you want to update
	*		  - double new_price -  the new price of the jewelry
	*		  - std::string new_collection -  the new collection of the jewelry
	* returns true if the jewelry was updated and false otherwise
	*/
	bool UpdateById(std::string updateId, double new_price, std::string new_collection);


	/*
	* PerformUndo- undoes the last operation performed on the repository
	* Returns true if the operation was undone and false if there was no operation to undo
	*/
	bool PerformUndo();
	
	/*
	* Redo- redoes the last undo operation performed on the repository
	* Returns true if the redo was performed and false if there was no undo operation to redo
	*/
	bool PerformRedo();

	/*
	* Adds to the repository all the objects from a txt file
	*/
	void addFromFile();

	/*
	* Adds to the repository all the objects from a csv file
	*/
	void addFromCSVFile();
	
	/*
	* Saves to a text file all the objects from the repository
	*/
	void saveToFile();

	/*
	* Saves to a csv file all the objects from the repository
	*/
	void saveToCSVFile();

	/*
	* Destructor for the controller class
	*/
	~Controller();

};

