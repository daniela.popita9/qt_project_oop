#include "proiect_oop_qt.h"
#include<QVBoxLayout>
#include<QTableWidget>
#include<QLabel>
#include<QTextEdit>
#include<QPushButton>
#include<QDebug>
#include<QFormLayout>
#include <QLineEdit>
#include <string.h>
#include <iostream>
#include <QMessageBox>
#include <QShortcut>


//using namespace std;


MainWindow::MainWindow(Controller& controller, QWidget* parent): QMainWindow(parent), GUI_controller(controller)
{
    this->GUI_controller.addFromCSVFile();
	this->createGUI();
}

void MainWindow::createGUI()
{
    this->setWindowTitle("Priceless");
    this->setMinimumSize(QSize(1400, 800));
    this->setMaximumSize(QSize(1800, 1200));
    this->setStyleSheet("background-color: Azure");
    QWidget* centralW = createWelcomeWidget();
    this->setCentralWidget(centralW);
}

void MainWindow::quitGUI()
{
    this->GUI_controller.saveToCSVFile();
    MainWindow::close();
}


// *** WELCOME MENU ***

QWidget* MainWindow::createWelcomeWidget()
{
    QWidget* widget = new QWidget(this);

    QWidget* menuProductW = createWelcomeWidgetElements();
    QVBoxLayout* vBoxLayout = new QVBoxLayout;

    vBoxLayout->addWidget(menuProductW);

    QFrame* line = new QFrame(this);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    vBoxLayout->addWidget(line);

    widget->setLayout(vBoxLayout);

    return widget;
}

QWidget* MainWindow::createWelcomeWidgetElements()
{
    QWidget* widget = new QWidget(this);

    QVBoxLayout* vBox = new QVBoxLayout(this);
    QLabel* title = new QLabel("P R I C E L E S S");
    QLabel* subtitle = new QLabel("Special jewelry for priceless people");

    QPushButton* buttonSeeCollection = new QPushButton("See our collection!", this);
    QPushButton* buttonQuit = new QPushButton("Quit", this);
    
    QFrame* line1 = new QFrame(this);
    line1->setFrameShape(QFrame::HLine);
    line1->setFrameShadow(QFrame::Sunken);
    QFrame* line2 = new QFrame(this);
    line2->setFrameShape(QFrame::HLine);
    line2->setFrameShadow(QFrame::Sunken);

    title->setStyleSheet("font-size: 60px; font-weight: bold; color: rgb(176, 48, 96)");
    subtitle->setStyleSheet("font-size: 15px; font: italic; color: rgb(219, 112, 147)");
    buttonSeeCollection->setStyleSheet("QPushButton:hover{font-size: 30px; color: rgb( 0,   0,   0); background-color: rgb(233, 150, 122)}"
        "QPushButton:!hover{font-size: 30px; color: rgb(0,0,0); background-color: rgb(176, 224, 230)}");
    buttonQuit->setStyleSheet("QPushButton:hover{font-size: 20px; color: rgb( 0,   0,   0); background-color: rgb(233, 150, 122)}"
        "QPushButton:!hover{font-size: 20px; color: rgb( 0,   0,   0); background-color: rgb(176, 224, 230)}");

    buttonSeeCollection->setFixedSize(400, 100);
    line1->setFixedWidth(200);
    line2->setFixedWidth(450);
    buttonQuit->setFixedSize(90, 40);

    vBox->addWidget(title);
    vBox->itemAt(0)->setAlignment(Qt::AlignCenter);
    vBox->addWidget(subtitle);
    vBox->itemAt(1)->setAlignment(Qt::AlignRight);
    vBox->addWidget(line1);
    vBox->itemAt(2)->setAlignment(Qt::AlignCenter);
    vBox->addWidget(buttonSeeCollection);
    vBox->itemAt(3)->setAlignment(Qt::AlignCenter);
    vBox->addWidget(line2);
    vBox->itemAt(4)->setAlignment(Qt::AlignCenter);
    vBox->addWidget(buttonQuit);
    vBox->itemAt(5)->setAlignment(Qt::AlignBaseline);

    vBox->setAlignment(Qt::AlignCenter);


    widget->setLayout(vBox);

    connect(buttonQuit, &QPushButton::pressed, this, &MainWindow::quitGUI);
    connect(buttonSeeCollection, &QPushButton::pressed, this, &MainWindow::createMenuShowAll);

    return widget;
}

// *** WELCOME MENU ***


// *** SHOW All MENU ***
void MainWindow::createMenuShowAll()
{
    QWidget* widget = new QWidget(this);

    QWidget* menuSelectW = createMenuWithElementsAndOptions();
    QVBoxLayout* vBoxLayout = new QVBoxLayout;

    vBoxLayout->addWidget(menuSelectW);

    widget->setLayout(vBoxLayout);
    this->setCentralWidget(widget);
    this->setMinimumSize(QSize(1400, 800));
    this->setMaximumSize(QSize(1800, 1200));

}

QWidget* MainWindow::createMenuWithElementsAndOptions()
{
    QWidget* widget = new QWidget(this);

    //list widget
    this->JewelryList = new QListWidget;
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllJewelry());
    this->JewelryList->setStyleSheet("font-size: 14px; font-weight: bold; color: rgb(95, 158, 160)");
    
    //buton widget
    QWidget* buttonLWidget = new QWidget;
    QGridLayout* buttonLLayout = new QGridLayout{};

    QVBoxLayout* vBox = new QVBoxLayout(this);
    
    QPushButton* buttonAddNecklace = new QPushButton("Add a necklace", this);
    QPushButton* buttonAddEarring = new QPushButton("Add earrings", this);
    QPushButton* buttonAddRing = new QPushButton("Add a ring", this);

    QPushButton* buttonRemove = new QPushButton("Remove", this);
    QPushButton* buttonFilter = new QPushButton("Filter", this);

    QPushButton* buttonUndo = new QPushButton("Undo", this);
    QPushButton* buttonBack = new QPushButton("<- Back <-", this);
    QPushButton* buttonRedo = new QPushButton("Redo", this);

    setPredifinedStyle(buttonAddNecklace);
    setPredifinedStyle(buttonAddEarring);
    setPredifinedStyle(buttonAddRing);
    setPredifinedStyle(buttonRemove);
    setPredifinedStyle(buttonFilter);
    setPredifinedStyle(buttonUndo);
    setPredifinedStyle(buttonBack);
    setPredifinedStyle(buttonRedo);

    buttonAddNecklace->setFixedHeight(40);
    buttonAddEarring->setFixedHeight(40);
    buttonAddRing->setFixedHeight(40);
    buttonRemove->setFixedHeight(40);
    buttonFilter->setFixedHeight(40);
    buttonUndo->setFixedHeight(30);
    buttonBack->setFixedHeight(30);
    buttonRedo->setFixedHeight(30);

    QFrame* line = new QFrame(this);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    buttonLLayout->addWidget(buttonAddNecklace, 1, 1);
    buttonLLayout->addWidget(buttonAddEarring, 1, 2);
    buttonLLayout->addWidget(buttonAddRing, 1, 3);
    buttonLLayout->addWidget(buttonRemove, 2, 1);
    buttonLLayout->addWidget(buttonFilter, 2, 3);
    buttonLLayout->addWidget(buttonUndo, 3,1);
    buttonLLayout->addWidget(buttonRedo, 3,3);

    vBox->addLayout(buttonLLayout);
    vBox->addWidget(line);
    vBox->addWidget(buttonBack);
    buttonLWidget->setLayout(vBox);

    connect(buttonBack, &QPushButton::pressed, this, &MainWindow::createGUI);
    connect(buttonAddNecklace, &QPushButton::pressed, this, &MainWindow::createAddNecklace);
    connect(buttonAddEarring, &QPushButton::pressed, this, &MainWindow::createAddEarrings);
    connect(buttonAddRing, &QPushButton::pressed, this, &MainWindow::createAddRing);
    connect(buttonRemove, &QPushButton::pressed, this, &MainWindow::createRemove);
    connect(buttonFilter, &QPushButton::pressed, this, &MainWindow::createFilterMenu);
    connect(buttonUndo, &QPushButton::pressed, this, &MainWindow::undo);
    connect(buttonRedo, &QPushButton::pressed, this, &MainWindow::redo);

    QShortcut* shortcut_undo = new QShortcut(QKeySequence("Ctrl+Z"), this);
    connect(shortcut_undo, &QShortcut::activated, this, &MainWindow::undo);

    QShortcut* shortcut_redo = new QShortcut(QKeySequence("Ctrl+Y"), this);
    connect(shortcut_redo, &QShortcut::activated, this, &MainWindow::redo);


    //final widget
    QVBoxLayout* finalVLayout = new QVBoxLayout;

    QLabel* displayTitle = new QLabel{ "These are all our jewelry:" };
    displayTitle->setStyleSheet("font-size: 15px; font: bold; color: rgb(176, 48, 96)");

    finalVLayout->addWidget(displayTitle);
    finalVLayout->addWidget(this->JewelryList);
    finalVLayout->addWidget(buttonLWidget);

    widget->setLayout(finalVLayout);
    return widget;
}

// *** END SHOW All MENU ***


// *** ADD OPTIONS ***
QWidget* MainWindow::createUniversalAddElements(int j_type)
{
    QWidget* widget = new QWidget(this);
    
    QVBoxLayout* VLayout = new QVBoxLayout();
    QFormLayout* formLayout = new QFormLayout();
    VLayout->addLayout(formLayout);

    this->MaterialInput = new QLineEdit();
    formLayout->addRow("Give one of the following options for the material : 1--GOLD, 2--SILVER, 3--PLATINUM", MaterialInput);

    this->PriceInput = new QLineEdit();
    formLayout->addRow("Give the price: ", PriceInput);

    this->WeightInput = new QLineEdit();
    formLayout->addRow("Give the weigth: ", WeightInput);

    this->GemstoneInput = new QLineEdit();
    formLayout->addRow("Give one of the following options: 0--NO GEMSTONE, 1--GEMSTONE", GemstoneInput);

    this->CollectionInput = new QLineEdit();
    formLayout->addRow("Give the colection", CollectionInput);

    if (j_type == 1)
    {
        this->ChainLengthInput = new QLineEdit();
        formLayout->addRow("Give the length of the chain: ", ChainLengthInput);
    }
    else if (j_type == 2)

    {
        this->IsPiercingInput = new QLineEdit();
        formLayout->addRow("Give one of the following options: 0--PAIR OF EARRINGS, 1--PIERCING", IsPiercingInput);

        this->EarringTypeInput = new QLineEdit();
        formLayout->addRow("Give one of the following options for the material : 1--STUDS, 2--HOOPS, 3--DROPS", EarringTypeInput);
    }
    else
    {
        this->RingSizeInput = new QLineEdit();
        formLayout->addRow("Give the size of the ring: ", RingSizeInput);
    }

    //buton widget
    QWidget* buttonLWidget = new QWidget;
    QGridLayout* buttonLLayout = new QGridLayout{};
    QVBoxLayout* vButtuonBox = new QVBoxLayout;

    QPushButton* buttonBack = new QPushButton("<- Back <-", this);
    QPushButton* buttonAdd = new QPushButton("ADD", this);
    QPushButton* buttonCancel = new QPushButton("Cancel", this);

    setPredifinedStyle(buttonCancel);
    setPredifinedStyle(buttonAdd);
    setPredifinedStyle(buttonBack);

    buttonCancel->setFixedHeight(40);
    buttonAdd->setFixedHeight(40);

    buttonLLayout->addWidget(buttonBack, 2, 1);
    buttonLLayout->addWidget(buttonCancel, 2, 3);
    buttonLLayout->addWidget(buttonAdd, 1, 2);;
    vButtuonBox->addLayout(buttonLLayout);
    buttonLWidget->setLayout(vButtuonBox);

    connect(buttonBack, &QPushButton::pressed, this, &MainWindow::createMenuShowAll);

    if (j_type == 1)
    {
        connect(buttonCancel, &QPushButton::pressed, this, &MainWindow::createAddNecklace);
        connect(buttonAdd, &QPushButton::pressed, this, &MainWindow::AddNecklace);

    }
    else if (j_type == 2)
    {
        connect(buttonCancel, &QPushButton::pressed, this, &MainWindow::createAddEarrings);
        connect(buttonAdd, &QPushButton::pressed, this, &MainWindow::AddEarrings);

    }
    else {
        connect(buttonCancel, &QPushButton::pressed, this, &MainWindow::createAddRing);
        connect(buttonAdd, &QPushButton::pressed, this, &MainWindow::AddRing);

    }
    //final widget
    QVBoxLayout* finalVLayout = new QVBoxLayout;

    QLabel* displayTitle = new QLabel{ "Here specify the characteristics of the Jewelry:" };
    displayTitle->setStyleSheet("font-size: 15px; font: italic; color: rgb(221, 160, 221)");

    finalVLayout->addWidget(displayTitle);
    finalVLayout->addLayout (VLayout);
    finalVLayout->addWidget(buttonLWidget);

    widget->setLayout(finalVLayout);
    return widget;
}


    // *** ADD NECKLACE ***
void MainWindow::createAddNecklace()
{
    QWidget* widget = new QWidget(this);

    QWidget* menuSelectW = createUniversalAddElements(1);
    QVBoxLayout* vBoxLayout = new QVBoxLayout;

    vBoxLayout->addWidget(menuSelectW);

    widget->setLayout(vBoxLayout);
    this->setCentralWidget(widget);
    this->setMinimumSize(QSize(1200, 800));
    this->setMaximumSize(QSize(1600, 1200));
    this->setStyleSheet("background-color: Azure");

}

void MainWindow::AddNecklace()
{
    float weight, length;
    double price;
    int aux_gemstone, materialcode;
    bool gemstone;
    std::string collection;
    Material material;


    try {
        weight = this->WeightInput->text().toFloat();
        length = this->ChainLengthInput->text().toFloat();
        price = this->PriceInput->text().toDouble();
        aux_gemstone = this->GemstoneInput->text().toInt();
        if (aux_gemstone == 1)
            gemstone = true;
        else if (aux_gemstone == 0)
            gemstone = false;
        else throw 1;
        materialcode = this->MaterialInput->text().toInt();
        material = MapToMaterial(materialcode);
        collection = CollectionInput->text().toStdString();
        this->GUI_controller.addNecklace(material, price, weight, gemstone, collection, length);
    }
    catch (int a)
    {
        QMessageBox::information(this, "Error", "the gemstone must be 1 or 0!");
    }
    catch (UnknownEnumCodeException e)
    {
        QMessageBox::information(this, "Error", e.what());
    }
    catch (NegativeNumberException n)
    {
        QMessageBox::information(this, "Error", "The price, weigth and length must be positive numbers!");
    }
    catch (TooHeavyWeightException w)
    {
        QMessageBox::information(this, "Error", "The weight shold be less than 500g!");
    }
    catch (...)
    {
        QMessageBox::information(this, "Error", "The price, weigth and length must be numbers!");
    }

    MainWindow::createMenuShowAll();
}
    // *** END ADD NECKLACE ***


    // *** ADD EARRINGS ***
void MainWindow::createAddEarrings()
{
    QWidget* widget = new QWidget(this);

    QWidget* menuSelectW = createUniversalAddElements(2);
    QVBoxLayout* vBoxLayout = new QVBoxLayout;

    vBoxLayout->addWidget(menuSelectW);

    widget->setLayout(vBoxLayout);
    this->setCentralWidget(widget);
    this->setMinimumSize(QSize(1200, 800));
    this->setMaximumSize(QSize(1600, 1200));
    this->setStyleSheet("background-color: Azure");

}

void MainWindow::AddEarrings()
{
    float weight;
    double price;
    int aux_gemstone, aux_ispiercing, materialcode, earringtypecode;
    bool gemstone, ispiercing;
    std::string collection;
    Material material;
    EaringType earringtype;


    try {
        weight = this->WeightInput->text().toFloat();
        price = this->PriceInput->text().toDouble();
        aux_gemstone = this->GemstoneInput->text().toInt();
        if (aux_gemstone == 1)
            gemstone = true;
        else if (aux_gemstone == 0)
            gemstone = false;
        else throw 1;
        materialcode = this->MaterialInput->text().toInt();
        Material material = MapToMaterial(materialcode);
        collection = CollectionInput->text().toStdString();

        aux_ispiercing = this->IsPiercingInput->text().toInt();
        if (aux_ispiercing == 1)
            ispiercing = true;
        else if (aux_ispiercing == 0)
            ispiercing = false;
        else throw 1;

        earringtypecode = this->EarringTypeInput->text().toInt();
        earringtype = MapToEaringType(earringtypecode);

        this->GUI_controller.addEarrings(material, price, weight, gemstone, collection, earringtype, ispiercing);
    }
    catch (int a)
    {
        QMessageBox::information(this, "Error", "the gemstone must be 1 or 0!");
    }
    catch (UnknownEnumCodeException e)
    {
        QMessageBox::information(this, "Error", e.what());
    }
    catch (NegativeNumberException n)
    {
        QMessageBox::information(this, "Error", "The price, weigth and length must be positive numbers!");
    }
    catch (TooHeavyWeightException w)
    {
        QMessageBox::information(this, "Error", "The weight shold be less than 500g!");
    }
    catch (...)
    {
        QMessageBox::information(this, "Error", "The price and length must be numbers!");
    }

    MainWindow::createMenuShowAll();
}
    // *** END ADD EARRINGS ***


    // *** ADD RING ***
void MainWindow::createAddRing()
{
    QWidget* widget = new QWidget(this);

    QWidget* menuSelectW = createUniversalAddElements(3);
    QVBoxLayout* vBoxLayout = new QVBoxLayout;

    vBoxLayout->addWidget(menuSelectW);

    widget->setLayout(vBoxLayout);
    this->setCentralWidget(widget);
    this->setMinimumSize(QSize(1200, 800));
    this->setMaximumSize(QSize(1600, 1200));
    this->setStyleSheet("background-color: Azure");

}

void MainWindow::AddRing()
{
    float weight, size;
    double price;
    int aux_gemstone, materialcode;
    bool gemstone;
    std::string collection;
    Material material;


    try {
        weight = this->WeightInput->text().toFloat();
        price = this->PriceInput->text().toDouble();
        aux_gemstone = this->GemstoneInput->text().toInt();
        if (aux_gemstone == 1)
            gemstone = true;
        else if (aux_gemstone == 0)
            gemstone = false;
        else throw 1;
        materialcode = this->MaterialInput->text().toInt();
        Material material = MapToMaterial(materialcode);
        collection = CollectionInput->text().toStdString();

        size=this->RingSizeInput->text().toInt();

        this->GUI_controller.addRing(material, price, weight, gemstone, collection, size);
    }
    catch (int a)
    {
        QMessageBox::information(this, "Error", "the gemstone must be 1 or 0!");
    }
    catch (UnknownEnumCodeException e)
    {
        QMessageBox::information(this, "Error", e.what());
    }
    catch (NegativeNumberException n)
    {
        QMessageBox::information(this, "Error", "The price, weigth and length must be positive numbers!");
    }
    catch (TooHeavyWeightException w)
    {
        QMessageBox::information(this, "Error", "The weight shold be less than 500g!");
    }
    catch (...)
    {
        QMessageBox::information(this, "Error", "The price, weigth and size must be numbers!");
    }

    MainWindow::createMenuShowAll();
}
    // *** END ADD RING ***


//*** END ADD OPTIONS ***


// *** REMOVE OPTION ***

void MainWindow::createRemove()
{
   
    QWidget* widget = new QWidget(this);

    QWidget* menuSelectW = createRemoveElements();
    QVBoxLayout* vBoxLayout = new QVBoxLayout;

    vBoxLayout->addWidget(menuSelectW);

    widget->setLayout(vBoxLayout);
    this->setCentralWidget(widget);
    this->setMinimumSize(QSize(400, 300));
    this->setMaximumSize(QSize(400, 300));
    this->setStyleSheet("background-color: Azure");
    this->setWindowTitle("REMOVE");
}

QWidget* MainWindow::createRemoveElements()
{
   QWidget* remove_widget = new QWidget(this);
   

   QWidget* buttonLWidget = new QWidget;
   QGridLayout* buttonsLayout = new QGridLayout{};
   QVBoxLayout* vButtuonBox = new QVBoxLayout;

   QPushButton* buttonOk = new QPushButton("Ok", this);
   QPushButton* buttonCancel = new QPushButton("Cancel", this);
   QPushButton* buttonBack = new QPushButton("<- Back <-", this);

   connect(buttonOk, &QPushButton::pressed, this, &MainWindow::removeByID);
   connect(buttonCancel, &QPushButton::pressed, this, &MainWindow::createRemove);
   connect(buttonBack, &QPushButton::pressed, this, &MainWindow::createMenuShowAll);

   buttonOk->setFixedHeight(20);
   buttonCancel->setFixedHeight(20);
   buttonBack->setFixedHeight(20);

   setPredifinedStyle(buttonOk);
   setPredifinedStyle(buttonCancel);
   setPredifinedStyle(buttonBack);

   QLabel* ID = new QLabel{ "Give the ID: " };
   this->IDForRemoveInput = new QLineEdit();

   buttonsLayout->addWidget(ID, 1, 1);
   buttonsLayout->addWidget(this->IDForRemoveInput,1,2);
   buttonsLayout->addWidget(buttonBack, 2, 1);
   buttonsLayout->addWidget(buttonCancel, 2,2 );
   buttonsLayout->addWidget(buttonOk, 2, 3);
   vButtuonBox->addLayout(buttonsLayout);
   buttonLWidget->setLayout(vButtuonBox);
   
   QVBoxLayout* finalVLayout = new QVBoxLayout;
   finalVLayout->addWidget(buttonLWidget);

   remove_widget->setLayout(finalVLayout);
   return remove_widget;

}

void MainWindow::removeByID()
{
    std::string removeID = this->IDForRemoveInput->text().toStdString();
    bool result = this->GUI_controller.RemoveById(removeID);

    std::string sucesfull_string_mesaj, wrong_string_mesaj;
    sucesfull_string_mesaj.append("The element of id : " + removeID + "was removed succefully!");
    wrong_string_mesaj.append("No element of id : " + removeID + "was found!");

    QString q_mesaj = QString::fromStdString(sucesfull_string_mesaj);


    QMessageBox* mesaj = new QMessageBox();
    if (result)
    {
        QString q_mesaj = QString::fromStdString(sucesfull_string_mesaj);
        mesaj->information(this, "SUCCESFULL", q_mesaj);
    }
    else
    {
        QString q_mesaj = QString::fromStdString(wrong_string_mesaj);
        mesaj->warning(this, "UNSUCCESFULL", q_mesaj);
    }
}

// *** END REMOVE OPTION ***



// *** FILTER OPTIONS ***
void MainWindow::createFilterMenu()
{
    QWidget* widget = new QWidget(this);

    QWidget* menuSelectW = createFilterMenuElements();
    QVBoxLayout* vBoxLayout = new QVBoxLayout;

    vBoxLayout->addWidget(menuSelectW);

    widget->setLayout(vBoxLayout);
    this->setCentralWidget(widget);

}

QWidget* MainWindow::createFilterMenuElements()
{
    QWidget* widget = new QWidget(this);

    //list widget
    this->JewelryList = new QListWidget;
    /*this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllJewelry());
    this->JewelryList->setStyleSheet("font-size: 15px; font-weight: bold; color: rgb(216, 191, 216)");
    */
    //buton widget
    QWidget* buttonLWidget = new QWidget;
    QGridLayout* buttonLLayout = new QGridLayout{};

    QVBoxLayout* vBox = new QVBoxLayout(this);

    QPushButton* buttonFilterNecklace = new QPushButton("See all necklaces", this);
    QPushButton* buttonFilterEarring = new QPushButton("See all earrings", this);
    QPushButton* buttonFilterRing = new QPushButton("See all rings", this);

    QPushButton* buttonFilterWithGemstone = new QPushButton("See all with gemstone", this);
    QPushButton* buttonFilterWithoutGemstone = new QPushButton("See all without gemstone", this);

    QPushButton* buttonAll = new QPushButton("See all", this);

    QPushButton* buttonBack = new QPushButton("<- Back <-", this);

    setPredifinedStyle(buttonFilterNecklace);
    setPredifinedStyle(buttonFilterEarring);
    setPredifinedStyle(buttonFilterRing);
    setPredifinedStyle(buttonFilterWithGemstone);
    setPredifinedStyle(buttonFilterWithoutGemstone);
    setPredifinedStyle(buttonBack);
    setPredifinedStyle(buttonAll);


    buttonFilterNecklace->setFixedHeight(40);
    buttonFilterEarring->setFixedHeight(40);
    buttonFilterRing->setFixedHeight(40);
    buttonFilterWithGemstone->setFixedHeight(40);
    buttonFilterWithoutGemstone->setFixedHeight(40);
    buttonAll->setFixedHeight(40);
    buttonBack->setFixedHeight(30);

    QFrame* line = new QFrame(this);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    buttonLLayout->addWidget(buttonFilterNecklace, 1, 1);
    buttonLLayout->addWidget(buttonFilterEarring, 1, 2);
    buttonLLayout->addWidget(buttonFilterRing, 1, 3);
    buttonLLayout->addWidget(buttonAll, 2, 1);
    buttonLLayout->addWidget(buttonFilterWithGemstone, 2, 2);
    buttonLLayout->addWidget(buttonFilterWithoutGemstone, 2, 3);

    vBox->addLayout(buttonLLayout);
    vBox->addWidget(line);
    vBox->addWidget(buttonBack);
    buttonLWidget->setLayout(vBox);

    connect(buttonBack, &QPushButton::pressed, this, &MainWindow::createMenuShowAll);
    connect(buttonFilterNecklace, &QPushButton::pressed, this, &MainWindow::filterNecklaces);
    connect(buttonFilterEarring, &QPushButton::pressed, this, &MainWindow::filterEarrings);
    connect(buttonFilterRing, &QPushButton::pressed, this, &MainWindow::filterRings);
    connect(buttonFilterWithGemstone, &QPushButton::pressed, this, &MainWindow::filterWithGemstone);
    connect(buttonFilterWithoutGemstone, &QPushButton::pressed, this, &MainWindow::filterWithoutGemstone);
    connect(buttonAll, &QPushButton::pressed, this, &MainWindow::filterAll);


    //final widget
    QVBoxLayout* finalVLayout = new QVBoxLayout;

    QLabel* displayTitle = new QLabel{ "The filtered jewelry are:" };
    displayTitle->setStyleSheet("font-size: 15px; font: bold; color: rgb(176, 48, 96)");

    finalVLayout->addWidget(displayTitle);
    finalVLayout->addWidget(this->JewelryList);
    finalVLayout->addWidget(buttonLWidget);

    widget->setLayout(finalVLayout);
    return widget;
}

void MainWindow::filterNecklaces()
{
    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllNecklaces());
}

void MainWindow::filterEarrings()
{
    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllEarrings());
}

void MainWindow::filterRings()
{
    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllRings());
}

void MainWindow::filterWithGemstone()
{
    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllJewelryWithGemstone());
}

void MainWindow::filterWithoutGemstone()
{
    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllJewelryWithoutGemstone());
}

void MainWindow::filterAll()
{
    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllJewelry());
}
// *** END FILTER OPTIONS ***


// *** UNDO FUNTION ***
void MainWindow::undo()
{
    bool result = this->GUI_controller.PerformUndo();
    if(result==false)
        QMessageBox::information(this, "Error", "Nothing to undo!");

    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllJewelry());
}

// *** REDO FUNTION ***
void MainWindow::redo()
{
    bool result = this->GUI_controller.PerformRedo();
    if (result == false)
        QMessageBox::information(this, "Error", "Nothing to redo!");

    this->JewelryList->clear();
    this->PopulateJewelryList(this->JewelryList, this->GUI_controller.getAllJewelry());

}


void MainWindow::setPredifinedStyle(QPushButton* button)
{
    //button->setStyleSheet("QPushButton:hover{font-size: 20px; color: rgb(61,165,202); background-color: rgb(196,240,255)}"
     //   "QPushButton:!hover{font-size: 20px; color: rgb(28,125,160); background-color: rgb(119,186,209)}");

    button->setStyleSheet("QPushButton:hover{font-size: 15px; color: rgb( 0,   0,   0); background-color: rgb(233, 150, 122)}"
        "QPushButton:!hover{font-size: 15px; color: rgb(0,0,0); background-color: rgb(176, 224, 230)}");

}

void MainWindow::PopulateJewelryList(QListWidget* to_this_list, std::vector<Jewelry*> vector)
{
    for (Jewelry* jewelry : vector)
    {
        if (dynamic_cast<Earrings*>(jewelry) != nullptr)
        {
            auto p = dynamic_cast<Earrings*>(jewelry);
            to_this_list->addItem(QString::fromStdString(p->DisplayJewelry()));
        }
        else if (dynamic_cast<Necklace*>(jewelry) != nullptr)
        {
            auto p = dynamic_cast<Necklace*>(jewelry);
            to_this_list->addItem(QString::fromStdString(p->DisplayJewelry()));
        }
        else if (dynamic_cast<Ring*>(jewelry) != nullptr)
        {
            auto p = dynamic_cast<Ring*>(jewelry);
            to_this_list->addItem(QString::fromStdString(p->DisplayJewelry()));
            //list << QString::fromStdString(p->DisplayJewelry());
        }
        else if (jewelry != nullptr)
        {
            //list << QString::fromStdString(jewelry->DisplayJewelry());
            to_this_list->addItem(QString::fromStdString(jewelry->DisplayJewelry()));
        }
    }
    if (to_this_list->count() == 0)
        to_this_list->addItem(QString::fromStdString("No items to display"));
    to_this_list->setStyleSheet("font-size: 14px; font-weight: bold; color: rgb(176, 48, 96);");

}


