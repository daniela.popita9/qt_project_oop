#include "Repository.h"

Repository::Repository()
{
	Jewelry::setNextId(1);
	Necklace::setNextNecklaceId(1);
	Ring::setNextRingId(1);
	Earrings::setNextEarringId(1);
}

Repository::Repository(const Repository& other)
{
	for (auto jewelry : other.get_jewelry_repo())
		this->jewelry_repo.push_back(jewelry);
}

Repository& Repository::operator=(const Repository& other)
{
	for (auto jewelry : other.get_jewelry_repo())
		this->jewelry_repo.push_back(jewelry);
	
	return *this;
}


void Repository::set_jewelry_repo(std::vector<Jewelry*> new_repo)
{
	this->jewelry_repo.clear();
	for (auto jewelry : new_repo)
		this->jewelry_repo.push_back(jewelry);
}


void Repository::add(Jewelry* newJewelry)
{
	this->undo_stack.push(this->jewelry_repo);
	this->jewelry_repo.push_back(newJewelry);
}


bool Repository::remove_by_id(std::string removeId)
{
	for (std::vector<Jewelry*>::iterator it = this->jewelry_repo.begin(); it != this->jewelry_repo.end(); it++)
	{
		if ((*it)->getId() == removeId) {
			this->undo_stack.push(this->jewelry_repo);
			Jewelry* item = (*it);
			this->jewelry_repo.erase(it);
			//delete item; // daca sterg pointer-ul nu imi merge undo-ul
			return true;
			break;
		}
	}
	return false;
}


bool Repository::update_by_id(std::string updateId, double new_price,  std::string new_collection)
{
	for (std::vector<Jewelry*>::iterator it = this->jewelry_repo.begin(); it != this->jewelry_repo.end(); it++)
	{
		if ((*it)->getId() == updateId) {
			this->undo_stack.push(this->jewelry_repo);
			Jewelry* item = (*it);
			item->updateJewelry(new_price, new_collection);
			return true;
			break;
		}
	}
	return false;
}


std::vector<Jewelry*> Repository::get_all_necklaces() const
{
	std::vector<Jewelry*>filtredVector;
	for (auto necklace : this->jewelry_repo)
		if (dynamic_cast<Necklace*>(necklace) != nullptr)
			filtredVector.push_back(necklace);

	return filtredVector;
}

std::vector<Jewelry*> Repository::get_all_necklaces_of_length(bool (*criteria)(double a, double b), float length) const
{
	std::vector<Jewelry*>filtredVector;

	for (auto necklace : this->jewelry_repo)
	{
		auto p = dynamic_cast<Necklace*>(necklace);
			if ((p!= nullptr) && criteria(p->getLength(), length))
				filtredVector.push_back(necklace);
	}
	return filtredVector;
}


std::vector<Jewelry*> Repository::get_all_rings() const
{
	std::vector<Jewelry*>filtredVector;
	for (auto ring : this->jewelry_repo)
		if (dynamic_cast<Ring*>(ring) != nullptr)
			filtredVector.push_back(ring);

	return filtredVector;
}

std::vector<Jewelry*> Repository::get_all_rings_of_size(bool (*criteria)(double a, double b), float size) const
{
	std::vector<Jewelry*>filtredVector;

	for (auto ring : this->jewelry_repo)
	{
		auto p = dynamic_cast<Ring*>(ring);
		if ((p != nullptr) && criteria(p->getSize(), size))
			filtredVector.push_back(ring);
	}
	return filtredVector;
}


std::vector<Jewelry*> Repository::get_all_earrings() const
{
	std::vector<Jewelry*>filtredVector;
	for (auto earring : this->jewelry_repo)
		if (dynamic_cast<Earrings*>(earring) != nullptr)
			filtredVector.push_back(earring);

	return filtredVector;
}

std::vector<Jewelry*> Repository::get_all_earrings_of_type(EaringType earringtype) const
{
	std::vector<Jewelry*>filtredVector;
	for (auto earring : this->jewelry_repo)
	{
		auto p = dynamic_cast<Earrings*>(earring);
		if ((p != nullptr) && (p->getEaringType() == earringtype))
			filtredVector.push_back(earring);
	}
	return filtredVector;
}

std::vector<Jewelry*> Repository::get_all_earrings_piercing_or_not(bool piercing_condition) const
{
	std::vector<Jewelry*>filtredVector;
	for (auto earring : this->jewelry_repo)
	{
		auto p = dynamic_cast<Earrings*>(earring);
		if ((p != nullptr) && p->getIsPiercing() == piercing_condition)
			filtredVector.push_back(earring);
	}
	return filtredVector;
}


std::vector<Jewelry*> Repository::get_by_material(Material material) const
{
	std::vector<Jewelry*>filtredVector;
	for (auto jewelry : this->jewelry_repo)
		if (jewelry->getMaterial() == material)
			filtredVector.push_back(jewelry);

	return filtredVector;
}

std::vector<Jewelry*> Repository::get_by_gemstone(bool gemstone_condition) const
{
	std::vector<Jewelry*>filtredVector;
	for (auto jewelry : this->jewelry_repo)
		if (jewelry->getGemstone() == gemstone_condition)
			filtredVector.push_back(jewelry);

	return filtredVector;
}

std::vector<Jewelry*> Repository::get_by_price(bool(*criteria)(double a, double b), double price) const
{
	std::vector<Jewelry*>filtredVector;
	for (auto jewelry : this->jewelry_repo)
		if (criteria(jewelry->getPrice(),price))
			filtredVector.push_back(jewelry);

	return filtredVector;
}

std::vector<Jewelry*> Repository::get_by_collection(std::string collection) const
{
	std::vector<Jewelry*>filtredVector;
	for (auto jewelry : this->jewelry_repo)
		if (jewelry->getCollection() == collection)
			filtredVector.push_back(jewelry);

	return filtredVector;
}


std::string Repository::to_string() const
{
	 std::string returnString;
	 for (auto jewelry : this->jewelry_repo)
		 returnString.append(jewelry->DisplayJewelry());
	 
	 return returnString;
}

std::ostream& operator<<(std::ostream& out, const Repository& repository)
{
	out << "This is my jewelry store: " << std::endl;

	for (auto jewelry : repository.get_jewelry_repo())
	{
		if (dynamic_cast<Earrings*>(jewelry) != nullptr)
		{
			auto p = dynamic_cast<Earrings*>(jewelry);
			out << *p;
		}
		else if (dynamic_cast<Necklace*>(jewelry) != nullptr)
		{
			auto p = dynamic_cast<Necklace*>(jewelry);
			out << *p;
		}
		else if (dynamic_cast<Ring*>(jewelry) != nullptr)
		{
			auto p = dynamic_cast<Ring*>(jewelry);
			out << *p;
		}
		else if(jewelry!=nullptr)
		{
			out << *jewelry;
		}
	}

	return out;
}


bool Repository::undo()
{
	if (this->undo_stack.size() != 0)
	{
		this->redo_stack.push(this->jewelry_repo);
		this->jewelry_repo = this->undo_stack.top();
		this->undo_stack.pop();
		return true;
	}
	return false;
}

bool Repository::redo()
{
	if (this->redo_stack.size() != 0)
	{
		this->undo_stack.push(this->jewelry_repo);
		this->jewelry_repo = this->redo_stack.top();
		this->redo_stack.pop();
		return true;
	}
	return false;
}

bool Repository::search(Jewelry* search_jewelry)
{
	bool ok = false;
	for (Jewelry* jewelry : this->jewelry_repo)
		if (jewelry == search_jewelry)
		{
			ok = true;
			break;
		}
	return ok;
}
Repository::~Repository()
{
	while (!this->undo_stack.empty())
	{
		std::vector < Jewelry* > delete_stack_vector = undo_stack.top();
		this->undo_stack.pop();
		for (Jewelry* jewelry : delete_stack_vector)
		{
			if (!this->search(jewelry))
			{
				this->add(jewelry);
			}
		}
		delete_stack_vector.clear();
	}

	while (!this->redo_stack.empty())
	{
		std::vector < Jewelry* > delete_stack_vector = redo_stack.top();
		this->redo_stack.pop();
		for (Jewelry* jewelry : delete_stack_vector)
		{
			if (!this->search(jewelry))
			{
				this->add(jewelry);
			}
		}
		delete_stack_vector.clear();

	}

	for (Jewelry* jewelry : this->jewelry_repo)
	{
		delete jewelry;
		jewelry = nullptr;
	}
	this->jewelry_repo.clear();

}