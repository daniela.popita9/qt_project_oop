#pragma once
#include <iostream>
#include <iomanip>
#include <string>
#include "NegativeNumberException.h"
#include "TooHeavyWeightException.h"
/*
* Material- enum
* 1- GOLD
* 2- SILVER
* 3- PLATINUM
*/
enum Material
{
	GOLD = 1,
	SILVER = 2,
	PLATINUM = 3
};
//Methoods for the Material enum:
/*
* 1. MapToMaterial- transforms a number into the coresponding material;
* Parameters: int - materialCode;
* Throws an exception of the materialCode is invalid;
*/
Material MapToMaterial(int materialCode);

/*
* 2. MapMaterialToString- returns the string version of the coresponding material;
* Parameters: Material - material;
* Throws an exception of the material is invalid;
*/
std::string MapMaterialToString(Material material) ;


class Jewelry
{
/*
* protected attributes
*/
protected:	

	/*	std::string id - the new id of the jewelry;*/
	std::string id;

	/*
	* Material material - the material of the jewelry(GOLD, SILVER or PLATINUM);
	*/
	Material material;

	/*
	* double price - the price of the jewelry;
	*			   - has to be positive
	*/
	double price;
	
	/*
	* float weight - the weight of the jewelry
	*			   - has to be less or equal to 500
	*/				;
	float weight;

	/*
	* bool gemstone- the gemstone condition for the jewelry(true if the jewelry has a gemstone, false otherwise);
	*/
	bool gemstone;

	/*
	* std::string id - the collection of the jewelry;
	*/
	std::string collection;

	/*
	* static int Nextid - generator for the id of every jewelry;
	*/
	static int Nextid;

/*
* public methoods
*/
public:
	/*
	* Constructors for the jewelery class:
	* Default constructor with no parameters
	*/
	Jewelry();

	/*
	* Constructor with parameters:
	* Params:
	* - Material- material- the material the jewelery is made from {GOLD, SILVER or PLATINUM}
	* - double - price- the price of the jewelery;
	*			    - has to be positive
	* - float  - weight- the weight of the jewelery;
	*		   - has to be less or equal to 500
	* - bool - gemstone -true if the jewelery has a gemstone and false otherwise;
	* - std:: string -collection- the collection the jewelery is part from;  
	*/
	Jewelry(Material material, double price, float weight, bool gemstone, std::string collection);

	/*
	* Copy constructor:
	* Params: Jewelry& other ;
	*/
	Jewelry(const Jewelry& other);
	
	/*
	* Overloaded assignment operator;
	* Params: Jewelry& other;
	*/
	Jewelry& operator =(const Jewelry& other);


	/*
	* methoods for the static atribute:
	* getToNextId- increases the id number by 1;
	*/
	static void getToNextId() { Nextid++; };

	/*
	* getNextId()- returns the current NextId;
	*/static int getNextId() { return Nextid; };

	/* 
	* setNextId- sets the nextID;
	* Params: int newNextId;
	* Throws an exception if the newNextId is not an integer;
	*/
	static void setNextId(int newNextId) { Nextid=newNextId; };

	/*
	* getter for the id- returns the id of the jewelry
	*/
	inline std::string getId() const { return this->id; };

	/*
	* getter for the material- returns the material of the jewelry;
	*/
	inline Material getMaterial() const { return this->material; };
	
	/*
	* setter for the material of the jewelry:
	*	Params: Material new_material - the new material for the jewelry;
	*/
	inline void setMaterial( Material new_material) { this->material= new_material; };

	/*
	*
	* getter for the price- returns the price of the jewelry
	*/
	inline double getPrice() const { return this->price; };;

	/*setter for the price of the jewelry:
		Params: double new_price - the new price for the jewelry;
	*/
	inline void setPrice(double new_price)
	{
		if (new_price <= 0)
			throw NegativeNumberException();
		this->price = new_price;
	};

	/*
	* getter for the weight- returns the weight of the jewelry;
	*/
	inline float getWeight() const { return this->weight; };

	/*setter for the weight of the jewelry:
		Params: float new_weight - the new weight for the jewelry;
	*/
	inline void setWeight(float new_weight) 
	{
		if (new_weight <= 0)
			throw NegativeNumberException();
		if (new_weight > 500)
			throw TooHeavyWeightException();
		this->weight = new_weight; 
	};

	/*getter for the gemstone atributte- returns the gemstone atributte of the jewelry;*/
	inline bool getGemstone() const { return this->gemstone; };
	
	/*setter for the gemstone of the jewelry:
	Parameters: bool new_gemstone - true if the jewelery has a gemstone and false otherwise;
	*/
	inline void setGemstone(bool new_gemstone) { this->gemstone = new_gemstone; };

	/*
	* getter for the collection- returns the collection of the jewelry;
	*/
	inline std::string getCollection() const { return this->collection; };
	
	/*
	* setter for the collection of the jewelry:
	* Params: bool new_collection - the new weight for the jewelry;
	*/
	inline void setCollection(std::string new_collection) { this->collection = new_collection; };

	/*	DisplayJewelry function, returns a string version of the object;
	*	Virtual methood;
	*/
	virtual std::string DisplayJewelry() const;
	/*
	* updates a jewelry - virtual methood
	* params: - double new_price -  the new price of the jewelry
	*		  - std::string new_collection -  the new collection of the jewelry
	*/
	virtual void updateJewelry(double new_price, std::string new_collection);

	/*
	* Overload for the ostream operator <<
	* Params - std::ostream& out- where you want to display the strig version of the jewelry,
	*		 - - Jewelry& jewelry - the jewelry you want to display;
	*/
	friend std::ostream& operator <<(std::ostream& out, const Jewelry& jewelry) ;


	/*
	* virtual function for display
	* Params:- std::ostream& out- the stream where you want to display the strig version of the earrings
	*/
	virtual void display(std::ostream& out)const;


	/*
	* Destructor for the jewelery class;
	* virtual methood
	*/
	virtual ~Jewelry();


};

