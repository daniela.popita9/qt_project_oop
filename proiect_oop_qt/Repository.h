#pragma once
#include "Jewelry.h"
#include "Necklace.h"
#include "Ring.h"
#include "Earrings.h"
#include <vector>
#include <stack>
using std::stack;


class Repository
{
//private attributes
private:
	
	//std::vector<Jewelry*> JewelryVector - a vector of objects of type Jewelry * (pointers to jewelry objects);
	std::vector<Jewelry*> jewelry_repo;
	
	//stack < std::vector < Jewelry* >> undo_stack-stack of vectors of objects of type Jewelry * (pointers to jewelry objects) for the undo operation;
	stack < std::vector < Jewelry* >> undo_stack;

	//stack < std::vector < Jewelry* >> undo_stack-stack of vectors of objects of type Jewelry * (pointers to jewelry objects) for the redo operation;
	stack < std::vector < Jewelry* >> redo_stack;


//public methoods
public:

	//Constructors for the repository class:
	//Default constructor with no parameters
	Repository();
	

	/*Copy constructor:
	Parameters: Repository& other;
	*/
	Repository(const Repository& other);
	

	/*Overloaded assignment operator;
	Parameters: Repository& other;
	*/
	Repository& operator =(const Repository& other);



	//getter for the jewelry_repo- returns jewelry_repo
	inline std::vector<Jewelry*> get_jewelry_repo() const { return this->jewelry_repo;};

	/*setter for the jewelry_repo:
		Parameters: std::vector<Jewelry*> new_repo - a vector of objects of type Jewelry*(pointers to jewelry objects);
	*/
	void set_jewelry_repo(std::vector<Jewelry*> new_repo) ;


	//getter for the jewelry_repo's size- returns the size of the repository, an integer;
	inline int getRepoSize() { return this->jewelry_repo.size(); };
	


	/*Add function for the repository
	*	Parameters: Jewelry* newJewelry- an object of type Jewelry*(pointer to an jewelry object)
	*/
	void add(Jewelry* newJewelry);


	/* get_by_material - returns a vector of all the objects that have a certain material from the repository
	* Parameters:- Material material - the material the jewelries need to have(GOLD, SILVER or PLATINUM)
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_by_material(Material material) const;


	/* get_by_gemstone - returns a vector of all the objects that have/ don't have a gemstone from the repository
	* Parameters:- gemstone_condition - true if the jewelry has a gemstone, false otherwise
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_by_gemstone(bool gemstone_condition) const;


	/* get_by_price - returns a vector of all the objects that have a price equal/less/ greater than a given price from the repository
	* Parameters:- double price - the price you want to comapre with
	*			 - bool (*criteria)(double a, double b) - a function with two parameters of type double(a,b) that gives the condition w.r.t the price(less, equal or greater)
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_by_price(bool (*criteria)(double a, double b), double price) const;


	/* get_by_collection - returns a vector of all the objects that are from the given collection from the repository
	* Parameters:- std::string collection - the collection from which you want the jewelry 
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_by_collection(std::string collection) const;



	/* get_all_necklaces - returns a vector of all the necklaces from the repository
=	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_all_necklaces() const;


	/* get_all_necklaces_of_length - returns a vector of all the necklaces that have a length equal/less/greater than a given length from the repository 
	* * Parameters:- float length - the length you want to comapre with
	*			   - bool (*criteria)(double a, double b) - a function with two parameters of type double(a,b) that gives the condition w.r.t the length(less, equal or greater)
=	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_all_necklaces_of_length(bool (*criteria)(double a, double b), float length) const;


	/* get_all_rings - returns a vector of all the rings from the repository
=	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_all_rings() const;


	/* get_all_rings_of_size - returns a vector of all the rings that have a size equal/less/greater than a given size from the repository
	* Parameters:- float size - the size you want to comapre with
	*			   - bool (*criteria)(double a, double b) - a function with two parameters of type double(a,b) that gives the condition w.r.t the size(less, equal or greater)
=	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_all_rings_of_size(bool (*criteria)(double a, double b), float size) const;



	/* get_all_earrings - returns a vector of all the earrings from the repository
=	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_all_earrings() const;


	/* get_all_earrings_of_type - rreturns a vector of all the earrings that have a certain type from the repository
	* Parameters:- EaringType earringtype - the type the earrings need to have(STUDS, HOOPS or DROPS);
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_all_earrings_of_type(EaringType earringtype) const;


	/* get_all_earrings_piercing_or_not - returns a vector of all the objects that are/aren't piercings from the repository
	* Parameters:- piercing_condition - true if the earring is a piercing, false otherwise
	* Returns: std::vector<Jewelry*>- a vector of objects of type Jewelry*(pointers to jewelry objects)
	*/
	std::vector<Jewelry*>get_all_earrings_piercing_or_not(bool piercing_condition) const;



	//	to_string function, returns a string version of the repository;
	std::string to_string() const;


	/*Overload for the ostream operator <<
		  Parametrs:
		  - std::ostream& out- where you want to display the strig version of the repository,
		  - Repository& repository - the repository you want to display;
	*/
	friend std::ostream& operator <<(std::ostream& out, const Repository& repository);
	
	/*Undo- undoes the last operation
	* Returns true if the operation was undone and false if there was no operation to undo
	*/
	bool undo();
	
	/*Redo- redoes the last undo operation
	* Returns true if the redo was performed and false if there was no undo operation to redo
	*/
	bool redo();

	/*remove_by_id -  removes a jewelery by the given id
	* Params: std::string removeId - the id of the jewelry you want to remove
	* Returns true if the jewelry was removed, false otherwise
	*/
	bool remove_by_id(std::string removeId);

	/*
	* updates a jewelry by id
	* params: - std::string updateId - the id of the jewelry that you want to update
	*		  - double new_price -  the new price of the jewelry
	*		  - std::string new_collection -  the new collection of the jewelry
	* returns true if the jewelry was updated and false otherwise
	*/
	bool update_by_id(std::string updateId, double new_price, std::string new_collection);
	
	/*
	* search - searches for a pointer to a jewelry in the current repository
	* params - Jewelry* search_jewelry -  a pointer to an object of type jewelry
	*
	* returns true if the pointer was found and false otherwise
	*/
	bool search(Jewelry* search_jewelry);
	
	//bool update_by_id(std::string updateId)
	//Destructor for the repository class;
	~Repository();

};

