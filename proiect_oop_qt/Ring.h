#pragma once
#include "Jewelry.h"
class Ring : public Jewelry
{

/*private attributes*/
private:

	/*float size - the size of the ring;*/
	float size;

	/*static int NextRingId - generator for the id of every ring;*/
	static int NextRingId;

/*public methoods*/
public:
	/*Constructors:*/
	/*Default constructor*/
	Ring();

	/*Constructor with parameters:
	* -Material- material- the material the ring is made from {GOLD, SILVER or PLATINUM}
	* -double - price- the price of the ring;
	* -float  - weight- the weight of the ring;
	* -bool - gemstone -true if the ring has a gemstone and false otherwise;
	* -std:: string -collection- the collection the ring is part from;
	* -float - size - the size of the ring;
	*/
	Ring(Material material, double price, float weight, bool gemstone, std::string collection, float size);

	/*Methood fot the static atribute*/
	/*GetToNextRingId increases the NextRingId*/
	static void GetToNextRingId() { NextRingId++; };

	/*getNextId()- returns the current NextId*/
	static int getNextRingId() { return NextRingId; };

	/*
	* setNextRingId- sets the nextRingID;
	* Params: int newNextRingId;
	* Throws an exception if the newNextRingId is not an integer;
	*/
	static void setNextRingId(int newNextRingId) { NextRingId = newNextRingId; };

	/*getter for the size- returns the size of the ring*/
	inline float getSize() const { return this->size; };

	/*
	* setter for the size of the ring:
	* Params: float new_size - the new size for the ring;
	*/
	inline void setSize(float new_size) 
	{ 
		if (new_size <= 0)
			throw NegativeNumberException();
		this->size = new_size;
	};

	/*	DisplayJewelry function, returns a string version of the object*/
	std::string DisplayJewelry() const;


	/*
	* virtual function for display
	* Params:- std::ostream& out- the stream where you want to display the strig version of the earrings
	*/
	void display(std::ostream& out)const override;

	/*
	* Overload for the ostream operator <<
	* Parametrs: - std::ostream& out- where you want to display the strig version of the ring,
	*			 - Ring& ring - the ring you want to display;
	*/
	friend std::ostream& operator <<(std::ostream& out, const Ring& ring);

};

