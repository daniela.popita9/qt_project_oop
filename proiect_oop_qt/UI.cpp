#include <string>
#include<iostream>
#include <iomanip>
#include <iostream>
#include "UI.h"
#include "proiect_oop_qt.h"
#include "NegativeNumberException.h"
#include "TooHeavyWeightException.h"
#include<QApplication>


int materialcode_validator()
{
	int materialcode;
	while (true)
	{
		std::cout << "Choose the material from the following: 1 Gold, 2 Silver, 3 Platinum: "; std::cin >> materialcode;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			throw UnknownEnumCodeException();
		}
		break;
	}
	return materialcode;
}

float weight_validator()
{
	float weight;
	while (true)
	{
		std::cout << "Give the weight of the jewelry: "; std::cin >> weight;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "THE WEIGHT SHOULD BE A NUMBER! " << std::endl;
			continue;
		}
		if (weight < 0)
		{
			throw NegativeNumberException();
		}
		if (weight > 500)
		{
			throw TooHeavyWeightException();
		}
		break;
	}
	return weight;
}

double price_validator()
{
	double price;
	while (true)
	{
		std::cout << "Give the price of the jewelry: "; std::cin >> price;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "THE PRICE SHOULD BE A NUMBER! " << std::endl;
			continue;
		}
		if (price < 0)
		{
			throw NegativeNumberException();
		}
		break;
	}
	return price;
}

bool gemstone_validator()
{
	bool gemstone;
	while (true)
	{
		std::cout << "Give 1 if the jewelry has a gemstone and 0 otherwise: "; std::cin >> gemstone;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "YOU MUST CHOOSE ONE OF THE PROVIDED OPTIONS!" << std::endl;
			continue;
		}
		break;
	}
	return gemstone;
}

int earringtypecode_validator()
{
	int earringtypecode;
	while (true)
	{
		std::cout << "Choose the type of the earrings from the following: 1 Studs, 2 Hoops, 3 Drops: "; std::cin >> earringtypecode;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			throw UnknownEnumCodeException();
		}
		break;
	}
	return earringtypecode;
}

bool ispiercing_validator()
{
	bool ispiercing;
	while (true)
	{
		std::cout << "Give 1 if the jewelry is a piercing or 0 if it is a pair of earrings: "; std::cin >> ispiercing;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "YOU MUST CHOOSE ONE OF THE PROVIDED OPTIONS!" << std::endl;
			continue;
		}
		break;
	}
	return ispiercing;
}

float size_validator()
{
	float size;
	while (true)
	{
		std::cout << "Give the size of the ring: "; std::cin >> size;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "THE SIZE SHOULD BE A NUMBER! " << std::endl;
			continue;
		}
		if (size < 0)
		{
			throw NegativeNumberException();
		}
		if (size > 70)
		{
			std::cout << "THE SIZE SHOLD BE LESS THAN 70!" << std::endl;
			continue;
		}
		break;
	}
	return size;
}

float length_validator()
{
	float length;
	while (true)
	{
		std::cout << "Give the size of the ring: "; std::cin >> length;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "THE LENGTH SHOULD BE A NUMBER! " << std::endl;
			continue;
		}
		if (length < 0)
		{
			throw NegativeNumberException();
		}
		if (length > 70)
		{
			std::cout << "THE LENGTH SHOLD BE LESS THAN 70" << std::endl;
			continue;
		}
		break;
	}
	return length;
}

std::string collection_vaidator()
{
	std::string collection;
	while (true)
	{
		std::cout << "Give the collection of the jewelry: ";
		std::cin.ignore();
		std::getline(std::cin, collection);
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "THE COLLECTION MUST BE A STRING!" << std::endl;
			continue;
		}
		break;
	}
	return collection;
}

std::string id_validator()
{
	std::string id;
	while (true)
	{
		std::cout << "Give the id of the jewelry: ";
		std::getline(std::cin, id);
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "THE ID MUST BE A STRING!" << std::endl;
			continue;
		}
		break;
	}
	return id;
}

void UI::showMenu() const
{
	std::cout << std::left << "Menu: " << std::endl;
	std::cout << std::left << std::setw(3) << "0." << " Exit" << std::endl;
	std::cout << std::left << std::setw(3) << "1." << " Add earring/s" << std::endl;
	std::cout << std::left << std::setw(3) << "2." << " Add a ring" << std::endl;
	std::cout << std::left << std::setw(3) << "3." << " Add a necklace" << std::endl;
	std::cout << std::left << std::setw(3) << "4." << " See all the jewelry" << std::endl;
	std::cout << std::left << std::setw(3) << "5." << " See all the jewelry made out of a material" << std::endl;
	std::cout << std::left << std::setw(3) << "6." << " See all the jewelry that have gemstones" << std::endl;
	std::cout << std::left << std::setw(3) << "7." << " See all the jewelry that don't have gemstones" << std::endl;
	std::cout << std::left << std::setw(3) << "8." << " See all the jewelry with price less than a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "9." << " See all the jewelry with price greater than a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "10." << " See all the jewelry with price equal to a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "11." << " See all the jewelry from a collection" << std::endl;
	std::cout << std::left << std::setw(3) << "12." << " See all the necklaces" << std::endl;
	std::cout << std::left << std::setw(3) << "13." << " See all the necklaces of lenght less than a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "14." << " See all the necklaces of lenght greater than a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "15." << " See all the necklaces of lenght equal to a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "16." << " See all the earrings" << std::endl;
	std::cout << std::left << std::setw(3) << "17." << " See all the earrings of a type" << std::endl;
	std::cout << std::left << std::setw(3) << "18." << " See all the pairs of earrings" << std::endl;
	std::cout << std::left << std::setw(3) << "19." << " See all the piercings" << std::endl;
	std::cout << std::left << std::setw(3) << "20." << " See all the rings" << std::endl;
	std::cout << std::left << std::setw(3) << "21." << " See all the rings of lenght less than a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "22." << " See all the rings of lenght greater than a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "23." << " See all the rings of lenght equal to a given value" << std::endl;
	std::cout << std::left << std::setw(3) << "24." << " Remove a jewelry by id:" << std::endl;
	std::cout << std::left << std::setw(3) << "25." << " Update a jewelry by id:" << std::endl;
	std::cout << std::left << std::setw(3) << "26." << " Undo the last operation:" << std::endl;
	std::cout << std::left << std::setw(3) << "27." << " Redo the last operation:" << std::endl;

}

void UI::printJewelryVector(std::vector<Jewelry*> JewelryVector) const
{
	for (auto jewelry : JewelryVector)
	{
		if (dynamic_cast<Earrings*>(jewelry) != nullptr)
		{
			auto p = dynamic_cast<Earrings*>(jewelry);
			std::cout << *p;
		}
		else if (dynamic_cast<Necklace*>(jewelry) != nullptr)
		{
			auto p = dynamic_cast<Necklace*>(jewelry);
			std::cout << *p;
		}
		else if (dynamic_cast<Ring*>(jewelry) != nullptr)
		{
			auto p = dynamic_cast<Ring*>(jewelry);
			std::cout << *p;
		}
		else if(jewelry != nullptr)
		{
			std::cout << *jewelry;
		}
	}

}

UI::UI()
{
}

UI::~UI()
{
}

void UI::run()
{
	///this->ctrl.addFromFile();
	this->ctrl.addFromCSVFile();
	while (true)
	{
		this->showMenu();
		int option;
		std::cout << "Give the option:\n>>"; std::cin >> option;
		if (option == 0)
		{
			//this->ctrl.saveToFile();
			this->ctrl.saveToCSVFile();
			std::cout << "Data succesfully saved to the file: MyCSVFile.csv! Good bye!\n";
			break;
		}
		else if (option == 1)
		{
			int materialcode, earringtypecode;
			double price;
			float weight;
			std::string collection;
			bool gemstone, ispiercing;
			try
			{
				materialcode = materialcode_validator();
				weight = weight_validator();
				price = price_validator();
				collection = collection_vaidator();
				gemstone = gemstone_validator();
				earringtypecode = earringtypecode_validator();
				ispiercing = ispiercing_validator();
				Material material = MapToMaterial(materialcode);
				EaringType earringtype = MapToEaringType(earringtypecode);
				this->ctrl.addEarrings(material, price, weight, gemstone, collection, earringtype, ispiercing);
			}
			catch (UnknownEnumCodeException e)
			{
				std::cout << e.what();
			}
			catch (NegativeNumberException n)
			{
				std::cout << "The value should be a positive number" << std::endl;
			}
			catch (TooHeavyWeightException t)
			{
				std::cout << "The weight shold be less than 500g" << std::endl;
			}

		}
		else if (option == 2)
		{
			int materialcode;
			double price;
			float weight, size;
			std::string collection;
			bool gemstone;
			try
			{
				materialcode = materialcode_validator();
				weight = weight_validator();
				price = price_validator();
				collection = collection_vaidator();
				gemstone = gemstone_validator();
				size = size_validator();
				Material material = MapToMaterial(materialcode);
				this->ctrl.addRing(material, price, weight, gemstone, collection, size);
			}
			catch (UnknownEnumCodeException e)
			{
				std::cout << e.what();
			}
			catch (NegativeNumberException n)
			{
				std::cout << "The value should be a positive number" << std::endl;
			}
			catch (TooHeavyWeightException t)
			{
				std::cout << "The weight shold be less than 500g" << std::endl;
			}
		}
		else if (option == 3)
		{
			int materialcode;
			double price;
			float weight, length;
			std::string collection;
			bool gemstone;
			try
			{
				materialcode = materialcode_validator();
				weight = weight_validator();
				price = price_validator();
				collection = collection_vaidator();
				gemstone = gemstone_validator();
				length = length_validator();
				Material material = MapToMaterial(materialcode);
				this->ctrl.addNecklace(material, price, weight, gemstone, collection, length);
			}
			catch (UnknownEnumCodeException e)
			{
				std::cout << e.what();
			}
			catch (NegativeNumberException n)
			{
				std::cout << "The value should be a positive number" << std::endl;
			}
			catch (TooHeavyWeightException t)
			{
				std::cout << "The weight shold be less than 500g" << std::endl;
			}

		}
		else if (option == 4)
		{
			std::cout << this->ctrl;
			//this->printJewelryVector(this->ctrl.getAllJewelry());
		}
		else if (option == 5)
		{
			int materialcode;
			try {
				materialcode = materialcode_validator();
				Material material = MapToMaterial(materialcode);
				this->printJewelryVector(this->ctrl.getAllJewelryMadeOfMaterial(material));
			}
			catch (UnknownEnumCodeException e)
			{
				std::cout << e.what();
			}
		}
		else if (option == 6)
		{
			this->printJewelryVector(this->ctrl.getAllJewelryWithGemstone());
		}
		else if (option == 7)
		{
			this->printJewelryVector(this->ctrl.getAllJewelryWithoutGemstone());
		}
		else if (option == 8)
		{
			double value;

			value = price_validator();
			this->printJewelryVector(this->ctrl.getAllJewelryWithPriceLessThan(value));

		}
		else if (option == 9)
		{
			double value;

			value = price_validator();
			this->printJewelryVector(this->ctrl.getAllJewelryWithPriceGreaterThan(value));

		}
		else if (option == 10)
		{
			double value;

			value = price_validator();
			this->printJewelryVector(this->ctrl.getAllJewelryWithPriceEqualTo(value));
		}
		else if (option == 11)
		{
			std::string collection;
			collection = collection_vaidator();
			this->printJewelryVector(this->ctrl.getAllJewelryFromCollection(collection));
		}
		else if (option == 12)
		{
			this->printJewelryVector(this->ctrl.getAllNecklaces());
		}
		else if (option == 13)
		{
			double value;

			value = price_validator();
			this->printJewelryVector(this->ctrl.getAllNecklacesOfLengthLessThan(value));

		}
		else if (option == 14)
		{
			double value;

			value = price_validator();
			this->printJewelryVector(this->ctrl.getAllNecklacesOfLengthGreaterThan(value));

		}
		else if (option == 15)
		{
			double value;

			value = price_validator();
			this->printJewelryVector(this->ctrl.getAllNecklacesOfLengthEqualTo(value));

		}
		else if (option == 16)
		{
			this->printJewelryVector(this->ctrl.getAllEarrings());
		}
		else if (option == 17)
		{
			int typecode;

			try {
				typecode = earringtypecode_validator();
				EaringType eType = MapToEaringType(typecode);
				this->printJewelryVector(this->ctrl.getAllEarringsOfType(eType));
			}
			catch (UnknownEnumCodeException e)
			{
				std::cout << e.what();
			}
		}
		else if (option == 18)
		{
			this->printJewelryVector(this->ctrl.getAllPairsOfEarrings());
		}
		else if (option == 19)
		{
			this->printJewelryVector(this->ctrl.getAllPiercings());
		}
		else if (option == 20)
		{
			this->printJewelryVector(this->ctrl.getAllRings());
		}
		else if (option == 21)
		{
			double value;

			value = size_validator();
			this->printJewelryVector(this->ctrl.getAllRingsOfSizeLessThan(value));

		}
		else if (option == 22)
		{
			double value;

			value = size_validator();
			this->printJewelryVector(this->ctrl.getAllRingsOfSizeGreaterThan(value));

		}
		else if (option == 23)
		{
			double value;

			value = size_validator();
			this->printJewelryVector(this->ctrl.getAllRingsOfSizeEqualTo(value));

		}
		else if (option == 24)
		{
			std::string id;
			bool result;
			std::cin.ignore();
			id = id_validator();
			result = this->ctrl.RemoveById(id);
			if (result)
				std::cout << "Item of id: " << id << " removed succesfully "<<std::endl;
			else
				std::cout << "No item of id " << id << " was found"<<std::endl;
		}
		else if (option == 25)
		{
			std::string collection, id;
			bool result;
			double price;
			
			std::cin.ignore();
			id = id_validator();
			price = price_validator();
			collection = collection_vaidator();
			result = this->ctrl.UpdateById(id, price, collection);
			if (result)
				std::cout << "Item of id: " << id << " updated succesfully " << std::endl;
			else
				std::cout << "No item of id " << id << " was found" << std::endl;

		}
		else if (option == 26)
		{
			bool result = this->ctrl.PerformUndo();
			if (result)
				std::cout << "Undo operation was succefull" << std::endl;
			else
				std::cout << "Nothing to undo" << std::endl;
		}
		else if (option == 27)
		{
			bool result = this->ctrl.PerformRedo();
			if (result)
				std::cout << "Redo operation was succefull" << std::endl;
			else
				std::cout << "Nothing to redo" << std::endl;
		}
		else 
		{
			std::cout << "Wrong option" << std::endl;
		}
		
	}
	
}

int UI::run_qt(int argc, char** argv)
{
	QApplication a(argc, argv);
	MainWindow w(this->ctrl);
	//MainWindow w;
	w.show();
	return a.exec();
}