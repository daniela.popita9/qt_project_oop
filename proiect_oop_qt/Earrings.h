#pragma once
#include "Jewelry.h"
/*
* EaringType- enum
* 1- STUDS
* 2- HOOPS
* 3- DROPS
*/
enum EaringType {
	STUDS=1,
	HOOPS=2,
	DROPS=3

};

/*Methoods for the EaringType enum:*/
/*
* MapToEaringType- transforms a number into the coresponding earing type
* Params: int - earingTypeCode
* Throws an exception of the earingTypeCode is invalid
*/
EaringType MapToEaringType(int earingTypeCode) ;

/*
* MapEaringTypeToString- returns the string version of the coresponding earing type
* Params: EaringType - earingType
* Throws an exception of the earingType is invalid
*/
std::string MapEaringTypeToString(EaringType earingType) ;


class Earrings : public Jewelry
{

/*private attributes*/
private:

	/*	EaringType type - the type of the earrings(STUDS, HOOPS or DROPS);*/
	EaringType etype;

	/*bool IsPiercing- the piercing condition for the earrings(true if the earring is a piercing, false otherwise);*/
	bool IsPiercing;

	/*static int NextEarringId - generator for the id of every earring/pair of earrings;*/
	static int NextEarringId;

/*public methoods*/
public:
	
	/*Constructors:*/
	/*Default constructor*/
	Earrings();
	
	/*
	* Constructor with parameters:
	* -Material- material- the material the earrings is made from {GOLD, SILVER or PLATINUM}
	* -double - price- the price of the earrings;
	* -float  - weight- the weight of the earrings;
	* -bool - gemstone -true if the earrings has a gemstone and false otherwise;
	* -std:: string -collection- the collection the earrings is part from;  
	* -EaringType type - the type of the earrings (STUDS, HOOPS or DROPS);
	* -bool- is piercing- false if the earrings is a pair of earings, true if it is a piercing
	*/ 
	Earrings(Material material, double price, float weight, bool gemstone, std::string collection,EaringType type, bool is_piercing);

	/*Methoods fot the static atribute*/
	/*GetToNextEarringId increases the NextEarringId*/
	static void GetToNextEarringId() { NextEarringId++; };

	/*getNextEarringId()- returns the current NextEarringId*/
	static int getNextEarringId() { return NextEarringId; };

	/*
	* setNextEarringId- sets the nextID;
	* Params: int newNextEarringId;
	* Throws an exception if the newNextEarringId is not an integer;
	*/
	static void setNextEarringId(int newNextEarringId) { NextEarringId = newNextEarringId; };


	/*getter for the type- returns the type of the earrings*/
	inline EaringType getEaringType() const { return this->etype; };

	/*
	* setter for the type of the earrings:
	* Params: float new_length - the new length for the earrings;
	*/
	inline void setEaringType(EaringType new_type) { this->etype = new_type; };

	/*getter for the isPiercing atribute- returns the type of the earrings*/
	inline bool getIsPiercing() const { return this->IsPiercing; };
	
	/*setter for the isPiercing atribute*/
	inline void setIsPiercing(bool new_IsPiercing) { this->IsPiercing = new_IsPiercing; };


	/*	DisplayJewelry function, returns a string version of the object*/
	std::string DisplayJewelry() const;

	/*
	* virtual function for display
	* Params:- std::ostream& out- the stream where you want to display the strig version of the earrings
	*/
	void display(std::ostream& out)const override;

	/*
	* Overload for the ostream operator <<
	* Params:- std::ostream& out- where you want to display the strig version of the earrings
	*		 - Earrings& earrings - the earrings you want to display 
	*/
	friend std::ostream& operator <<(std::ostream& out, const Earrings& earrings);
};

