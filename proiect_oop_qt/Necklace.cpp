#include "Necklace.h"

int Necklace::NextNecklaceId = 1;

Necklace::Necklace() : Jewelry()
{
	this->id.append("N" + std::to_string(NextNecklaceId));
}

Necklace::Necklace( Material material, double price, float weight, bool gemstone, std::string collection, float length) : Jewelry( material, price, weight, gemstone, collection)
{
	if (length <= 0)
		throw NegativeNumberException();
	this->length = length;
	this->id.append("N" + std::to_string(NextNecklaceId));
}



std::string Necklace::DisplayJewelry() const
{
	std::string stringVersion = Jewelry::DisplayJewelry();
	stringVersion.append("This is a necklace of lenght " + std::to_string(this->length)+'\n');

	return stringVersion;
}

void Necklace::display(std::ostream& out) const
{
	Jewelry::display(out);
	out << "This is a necklace of lenght " << std::setw(4) << std::setprecision(4) << this->length << '\n' << std::endl;
}

std::ostream& operator<<(std::ostream& out, const Necklace& necklace)
{
	out << "Jewelry of id: " << std::setw(6) << std::right << necklace.getId() << ". It is made from: "<< std::setw(9) <<std::left << MapMaterialToString(necklace.getMaterial());
	if (necklace.getGemstone())
		out << std::setw(27) << "it has a gemstone";
	else {
		out << std::setw(27) << "it doesn't have a gemstone";
	}
	out << " it weights " << std::setw(3) << std::setprecision(2) << necklace.getWeight() << " grams, it costs " << std::right << std::setw(8) << std::setprecision(8) << necklace.getPrice() << "$, " << "and is part of " << std::setw(20) << necklace.getCollection() << " collection. ";

	out << "This is a necklace of lenght " << std::setw(4) << std::setprecision(4) << necklace.length << '\n' << std::endl;
	return out;
}
