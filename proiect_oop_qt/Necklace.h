#pragma once
#include "Jewelry.h"

class Necklace : public Jewelry
{

/*private attributes*/
private:

	/*float length - the length of the necklace;*/
	float length;

	/*static int NextNecklaceId - generator for the id of every necklace;*/
	static int NextNecklaceId;

/*public methoods*/
public:
	/*Constructors:*/
	/*Default constructor*/
	Necklace();

	/*
	* Constructor with parameters:
	* -Material- material- the material the necklace is made from {GOLD, SILVER or PLATINUM}
	* -double - price- the price of the necklace;
	* -float  - weight- the weight of the necklace;
	* -bool - gemstone -true if the necklace has a gemstone and false otherwise;
	* -std:: string -collection- the collection the necklace is part from;
	* -float- length - the length of the necklace
	*/
	Necklace (Material material, double price, float weight, bool gemstone, std::string collection, float length);

	/*Methoods fot the static atribute*/
	/*GetToNextNecklaceId increases the NextNecklaceId*/
	static void GetToNextNecklaceId() { NextNecklaceId++; };

	/*getNextNecklaceId()- returns the current NextNecklaceId*/
	static int getNextNecklaceId() { return NextNecklaceId; };

	/*
	* setNextNecklaceId- sets the nextNecklaceID;
	* Params: int newNextNecklaceId;
	* Throws an exception if the newNextNecklaceId is not an integer;
	*/
	static void setNextNecklaceId(int newNextNecklaceId) { NextNecklaceId = newNextNecklaceId; };

	/*getter for the length- returns the length of the necklace*/
	inline float getLength() const { return this->length; };

	/*
	* setter for the length of the necklace:
	* Params: float new_length - the new length for the necklace;
	*/
	inline void setLength(float new_length) 
	{ 
		if (new_length <= 0)
			throw NegativeNumberException();
		this->length = new_length; 
	};


	/* DisplayJewelry function, returns a string version of the object*/
	std::string DisplayJewelry() const;


	/*
	* virtual function for display
	* Params:- std::ostream& out- the stream where you want to display the strig version of the earrings
	*/
	void display(std::ostream& out)const override;

	/*
	* Overload for the ostream operator <<
	* Parametrs: - std::ostream& out- where you want to display the strig version of the necklace,
	*	         - Necklace& necklace - the necklace you want to display;
	*/
	friend std::ostream& operator <<(std::ostream& out, const Necklace& necklace);


};

