#pragma once

/*#ifndef PROJECT_OOP_QT_MAINWINDOW_H
#define PROJECT_OOP_QT_MAINWINDOW_H
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include "ui_proiect_oop_qt.h"
#include <QVector>
#include <QListWidget>
#include <QWidget>
#include "Controller.h"
/*
* MainWindow class for the GUI
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:

    /*slot for creating the GUI*/
    void createGUI();

    /*slot for quitting the GUI*/
    void quitGUI();

    /*slot for creating the main menu*/
    void createMenuShowAll();
    
    /*slot for creating the menu for adding a necklace*/
    void createAddNecklace();
    /*slot for creating the menu for adding earrings*/
    void createAddEarrings();
    /*slot for creating the menu for adding a ring*/
    void createAddRing();

    /*slot for creating the remove window*/
    void createRemove();
    
    /*slot for creating the menu for filtering*/
    void createFilterMenu();


private:

    /*object of type controller*/
    Controller& GUI_controller;
    
    /*a pointer to an object of type QListWidget*/
    QListWidget* JewelryList;

    /*created the window for the welcome
    * returns an object of type QWidget
    */
    QWidget* createWelcomeWidget();
    
    /*created the widget elements for the welcome
    * returns an object of type QWidget
    */
    QWidget* createWelcomeWidgetElements();


    /*created the widget elements for all the options of the main menu
    * returns an object of type QWidget
    */
    QWidget* createMenuWithElementsAndOptions();
  
    /*created the widget elements for all the addding options of the main menu
    * params: j_type: 1--add a anecklace, 2--add earrrings, 3--add a ring
    * returns an object of type QWidget
    */
    QWidget* createUniversalAddElements(int j_type);

    /*created the widget elements for the remove option
    * returns an object of type QWidget
    */
    QWidget* createRemoveElements();

    /*created the widget elements for all the filtering options
    * returns an object of type QWidget
    */
    QWidget* createFilterMenuElements();

    /*a pointer to an object of type QLineEdit for add*/
    QLineEdit* MaterialInput;
    /*a pointer to an object of type QLineEdit for add*/
    QLineEdit* PriceInput;
    /*a pointer to an object of type QLineEdit for add*/
    QLineEdit* WeightInput;
    /*a pointer to an object of type QLineEdit for addi*/
    QLineEdit* GemstoneInput;
    /*a pointer to an object of type QLineEdit for addi*/
    QLineEdit* CollectionInput;
    /*a pointer to an object of type QLineEdit for addi*/
    QLineEdit* ChainLengthInput;
    /*a pointer to an object of type QLineEdit for addi*/
    QLineEdit* EarringTypeInput;
    /*a pointer to an object of type QLineEdit for addi*/
    QLineEdit* IsPiercingInput;
    /*a pointer to an object of type QLineEdit for addi*/
    QLineEdit* RingSizeInput;
    /*a pointer to an object of type QLineEdit for addi*/
    QLineEdit* IDForRemoveInput;

    /* auxiliar function for adding, creates and adds to the controller a new Neckalace with the provided properties*/
    void AddNecklace();
    /* auxiliar function for adding, creates and adds to the controller a new Earring with the provided properties*/
    void AddEarrings();
    /* auxiliar function for adding, creates and adds to the controller a new Ring with the provided properties*/
    void AddRing();

    /* auxiliar function for removing, does the removing part for the controller*/
    void removeByID();

    /* auxiliar function for filtering, collects all the necklaces from the controller*/
    void filterNecklaces();
    /* auxiliar function for filtering, collects all the earrings from the controller*/
    void filterEarrings();
    /* auxiliar function for filtering, collects all the rings from the controller*/
    void filterRings();
    /* auxiliar function for filtering, collects all the jewelry from the controller*/
    void filterAll();
    /* auxiliar function for filtering, collects all the jewelry with gemstone from the controller*/
    void filterWithGemstone();
    /* auxiliar function for filtering, collects all the jewelry without gemstone from the controller*/
    void filterWithoutGemstone();

    /* calls the controller to perforn undo*/
    void undo();
    /* calls the controller to perforn redo*/
    void redo();

    /* sets the style of a given button
    * params: QPushButton* button-  a pointer to an object of type QPushButton
    */
    void setPredifinedStyle(QPushButton* button);


public:
    
    /*constructor for the main window
    * params: Controller& controller -  the controler of to the application that needs GUI interface
    *         QWidget* parent - the parent of the current window( is by default null) 
    */
    MainWindow(Controller& controller, QWidget* parent = nullptr );

    /*adds all the items from a vector to a QListWidget
    * params: QListWidget* to_this_list -  a pointe to an object of type QListWidget
    *         std::vector<Jewelry*> vector - a vector of objects of type pointer to Jewelry
    */
    void PopulateJewelryList(QListWidget* to_this_list, std::vector<Jewelry*> vector);

};

#endif

