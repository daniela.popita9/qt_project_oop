#include "Ring.h"

int Ring::NextRingId = 1;

Ring::Ring() : Jewelry()
{
	this->id.append("R" + std::to_string(NextRingId));
}

Ring::Ring(Material material, double price, float weight, bool gemstone, std::string collection, float size) : Jewelry(material, price, weight, gemstone, collection )
{
	if (size <= 0)
		throw NegativeNumberException();
	this->size = size;
	this->id.append("R" + std::to_string(NextRingId));
}

std::string Ring::DisplayJewelry() const
{
	std::string stringVersion = Jewelry::DisplayJewelry();
	stringVersion.append("This is a ring of size " + std::to_string( this->size ) + '\n');

	return stringVersion;
}

void Ring::display(std::ostream& out) const
{
	Jewelry::display(out);
	out << "This is a ring of size " << std::setw(3) << std::setprecision(3) << this->size << std::endl << std::endl;

}



std::ostream& operator<<(std::ostream& out, const Ring& ring)//
{
	out << "Jewelry of id: " << std::setw(6) << std::right << ring.getId() << ". It is made from: " << std::setw(9) << std::left << MapMaterialToString(ring.getMaterial());
	if (ring.getGemstone())
		out << std::setw(27) << "it has a gemstone";
	else {
		out << std::setw(27) << "it doesn't have a gemstone";
	}
	out << " it weights " << std::setw(3) << std::setprecision(2) << ring.getWeight() << " grams, it costs " << std::right << std::setw(8) << std::setprecision(8) << ring.getPrice() << "$, " << "and is part of " << std::setw(20) << ring.getCollection() << " collection. ";

	out << "This is a ring of size " << std::setw(3) << std::setprecision(3) << ring.size << std::endl<< std::endl;

	return out;
}
