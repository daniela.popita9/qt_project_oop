#include "Earrings.h"
#include <exception>
#include "UnknownEnumCodeException.h"


EaringType MapToEaringType(int earingTypeCode)
{
	switch (earingTypeCode)
	{
	case 1:
		return STUDS;
		break;
	case 2:
		return HOOPS;
		break;
	case 3:
		return DROPS;
		break;
	default:
		throw UnknownEnumCodeException();
		break;
	}

}

std::string MapEaringTypeToString(EaringType earingType)
{

	switch (earingType)
	{
	case STUDS:
		return "studs";
		break;
	case HOOPS:
		return "hoops";
		break;
	case DROPS:
		return "drops";
		break;
	default:
		throw UnknownEnumCodeException();
		break;
	}
}


int Earrings::NextEarringId = 1;

Earrings::Earrings() : Jewelry()
{
	this->id.append("E" + std::to_string(NextEarringId));
}

Earrings::Earrings (Material material, double price, float weight, bool gemstone, std::string collection, EaringType type, bool is_piercing) : Jewelry(material, price, weight, gemstone, collection)
{
	this->IsPiercing = is_piercing;
	this->etype = type;
	this->id.append("E" + std::to_string(NextEarringId));
}



std::string Earrings::DisplayJewelry() const
{
	std::string stringVersion = Jewelry::DisplayJewelry();
	if (this->IsPiercing)
		stringVersion.append("This is a piercing of type ");
	else
		stringVersion.append("This is a pair of earrings of type ");
	
	stringVersion.append(MapEaringTypeToString(this->etype)+'\n');

	return stringVersion;
}

void Earrings::display(std::ostream& out) const
{
	Jewelry::display(out);
	if (this->IsPiercing)
		out << std::left << "This is " << std::setw(18) << "a piercing" << " of type ";
	else
		out << std::left << "This is " << std::setw(18) << "a pair of earrings" << " of type ";

	out << std::setw(5) << MapEaringTypeToString(this->etype) << '\n' << std::endl;

}



std::ostream& operator<<(std::ostream& out, const Earrings& earrings)
{
	out << "Jewelry of id: " << std::setw(6) << std::right << earrings.getId() << ". It is made from: " << std::setw(9) << std::left << MapMaterialToString(earrings.getMaterial());
	if (earrings.getGemstone())
		out << std::setw(27) << "it has a gemstone";
	else {
		out << std::setw(27) << "it doesn't have a gemstone";
	}
	out << " it weights " << std::setw(3) << std::setprecision(2) << earrings.getWeight() << " grams, it costs " << std::right << std::setw(8) << std::setprecision(8) << earrings.getPrice() << "$, " << "and is part of " << std::setw(20) << earrings.getCollection() << " collection. ";

	if (earrings.IsPiercing)
		out <<std::left << "This is "<<  std::setw(18) <<"a piercing" << " of type ";
	else
		out << std::left << "This is " << std::setw(18) <<"a pair of earrings" << " of type ";

	out <<std::setw(5) << MapEaringTypeToString(earrings.etype) << '\n'<<std::endl;

	return out;
}
