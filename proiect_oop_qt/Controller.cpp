#include "Controller.h"
#include<iostream>
#include<fstream>

using namespace std;

auto isequal{ [](double a, double b) { return a == b; } };

auto isdecreasing{ [](double a, double b) { return a >= b; } };

auto isincreasing{ [](double a, double b) { return a <= b; } };

Controller::Controller()
{
}

bool Controller::RemoveById(std::string removeId)
{
	return this->repository.remove_by_id(removeId);
}

bool Controller::UpdateById(std::string updateId, double new_price, std::string new_collection)
{
	return this->repository.update_by_id(updateId,  new_price, new_collection);
}


bool Controller::PerformUndo()
{
	return this->repository.undo();
}

bool Controller::PerformRedo()
{
	return this->repository.redo();
}


void Controller::addFromFile()
{
	string fileName = "read.txt";
	ifstream read_file(fileName);
	std::string s;
	//std::getline(read_file, s);
	while (!read_file.eof()) 
	{
		std::string materialcode, collection, earringtypecode, jewelrytype, s;
		std::getline(read_file, jewelrytype);
		std::getline(read_file, materialcode);
		Material material = MapToMaterial(std::stoi(materialcode));

		double price, weight, length, size;
		bool gemstone, ispiercing;
		
		std::getline(read_file, s);
		price = std::stoi(s);

		std::getline(read_file, s);
		weight = std::stoi(s);

		std::getline(read_file, s);
		gemstone = std::stoi(s);

		std::getline(read_file, collection);

		if (jewelrytype == "Necklace")
		{
			//read_file >> length;
			std::getline(read_file, s);
			length = std::stoi(s);

			Jewelry* newJewelry = new Necklace(material, price, weight, gemstone, collection, length);
			this->repository.add(newJewelry);
			Jewelry::getToNextId();
			Necklace::GetToNextNecklaceId();
		}
		else if (jewelrytype == "Ring")
		{
			//read_file >> size;
			std::getline(read_file, s);
			size = std::stoi(s);

			Jewelry* newJewelry = new Ring(material, price, weight, gemstone, collection, size);
			this->repository.add(newJewelry);
			Jewelry::getToNextId();
			Ring::GetToNextRingId();
		}
		else {
			std::getline(read_file, earringtypecode);
			
			std::getline(read_file, s);
			ispiercing = std::stoi(s);


			EaringType type = MapToEaringType(std::stoi(earringtypecode));
			Jewelry* newJewelry = new Earrings(material, price, weight, gemstone, collection, type, ispiercing);
			this->repository.add(newJewelry);
			Jewelry::getToNextId();
			Earrings::GetToNextEarringId();
		}
	}
}

void Controller::addFromCSVFile()
{
	string fileName = "MyCSVFile.csv";
	ifstream read_file(fileName);
	std::string s, line, materialcode, collection, earringtypecode, jewelrytype, delimiter =",";
	int position = 0;
	double price, weight, length, size;
	bool gemstone, ispiercing;

	while (!read_file.eof())
	{
		while (std::getline(read_file, line))
		{
			position = line.find(delimiter); 
			jewelrytype = line.substr(0, position);
			line = line.erase(0, position + 1);

			position = line.find(delimiter); 
			materialcode = line.substr(0, position); 
			line = line.erase(0, position + 1);

			Material material;
			if (materialcode == "gold")
				material = GOLD;
			else if (materialcode == "platinum")
				material = PLATINUM;
			else
				material = SILVER;

			position = line.find(delimiter); 
			s = line.substr(0, position); 
			line = line.erase(0, position + 1);
			price = std::stod(s);

			position = line.find(delimiter); 
			s = line.substr(0, position);
			line = line.erase(0, position + 1);
			weight = std::stof(s);

			position = line.find(delimiter); 
			s = line.substr(0, position); 
			line = line.erase(0, position + 1);
			if (s == "true")
				gemstone = 1;
			else
				gemstone = 0;

			position = line.find(delimiter); 
			collection = line.substr(0, position); 
			line = line.erase(0, position + 1);

			if (jewelrytype == "Necklace")
			{
				position = line.find(delimiter); 
				s = line.substr(0, position); 
				line = line.erase(0, position + 1);
				length = std::stoi(s);

				Jewelry* newJewelry = new Necklace(material, price, weight, gemstone, collection, length);
				this->repository.add(newJewelry);
				Jewelry::getToNextId();
				Necklace::GetToNextNecklaceId();
			}
			else if (jewelrytype == "Ring")
			{
				position = line.find(delimiter); 
				s = line.substr(0, position); 
				line = line.erase(0, position + 1);
				size = std::stoi(s);

				Jewelry* newJewelry = new Ring(material, price, weight, gemstone, collection, size);
				this->repository.add(newJewelry);
				Jewelry::getToNextId();
				Ring::GetToNextRingId();
			}
			else {
				position = line.find(delimiter); 
				earringtypecode = line.substr(0, position); 
				line = line.erase(0, position + 1);				
				
				EaringType type;
				if (earringtypecode == "drops")
					type = DROPS;
				else if (earringtypecode == "hoops")
					type = HOOPS;
				else
					type = STUDS;

				position = line.find(delimiter); 
				s = line.substr(0, position); 
				line = line.erase(0, position + 1);
				if (s == "true")
					ispiercing = 1;
				else
					ispiercing = 0;

				Jewelry* newJewelry = new Earrings(material, price, weight, gemstone, collection, type, ispiercing);
				this->repository.add(newJewelry);
				Jewelry::getToNextId();
				Earrings::GetToNextEarringId();
			}
		}
	}
}


void Controller::saveToFile()
{

	vector<Jewelry*> FileJewelry = this->repository.get_jewelry_repo();

	std::string fileName = "data.out.txt";

	ofstream out(fileName);

	for (Jewelry* jewelry : FileJewelry) {
		out << *jewelry;

	}

}

void Controller::saveToCSVFile()
{
	std::string fileName = "MyCSVFile.csv";

	ofstream out(fileName);

	for (Jewelry* jewelry : this->repository.get_jewelry_repo())
	{
		auto p = dynamic_cast<Necklace*>(jewelry);
		if (p != nullptr)
		{
			out << "Necklace" << "," << MapMaterialToString(p->getMaterial()) << "," << p->getPrice() << "," << p->getWeight() << ",";
			if (p->getGemstone())
				out << "true" << ",";
			else out << "false" << ",";

			out << p->getCollection() << "," << p->getLength() << std::endl;
		}
		
		else {
			auto p = dynamic_cast<Earrings*>(jewelry);
			if (p != nullptr)
			{
				out << "Earrings" << "," << MapMaterialToString(p->getMaterial()) << "," << p->getPrice() << "," << p->getWeight() << ",";
				if (p->getGemstone())
					out << "true" << ",";
				else out << "false" << ",";

				out << p->getCollection() << "," << MapEaringTypeToString
				(p->getEaringType()) << ",";
				if (p->getIsPiercing())
					out << "true" << ",";
				else out << "false" << ",";
				out << std::endl;
			}
			else {
				auto p = dynamic_cast<Ring*>(jewelry);
				if (p != nullptr)
				{
					out << "Ring" << "," << MapMaterialToString(p->getMaterial()) << "," << p->getPrice() << "," << p->getWeight() << ",";
					if (p->getGemstone())
						out << "true" << ",";
					else out << "false" << ",";

					out << p->getCollection() << "," << p->getSize() << std::endl;


				}
			}
		}
		

	}

}


Controller::~Controller()
{
}

void Controller::addEarrings( Material material, double price, float weight, bool gemstone, std::string collection, EaringType type, bool is_piercing)
{
	Jewelry* newEarrings = new Earrings( material, price, weight, gemstone, collection, type, is_piercing);
	this->repository.add(newEarrings);
	
	Jewelry::getToNextId();
	Earrings::GetToNextEarringId();
}

void Controller::addNecklace( Material material, double price, float weight, bool gemstone, std::string collection, float length)
{
	Jewelry* newNecklace = new Necklace( material, price, weight, gemstone, collection, length);
	this->repository.add(newNecklace);

	Jewelry::getToNextId();
	Necklace::GetToNextNecklaceId();
}

void Controller::addRing( Material material, double price, float weight, bool gemstone, std::string collection, float size)
{
	Jewelry* newRing = new Ring( material, price, weight, gemstone, collection, size);
	this->repository.add(newRing);

	Jewelry::getToNextId();
	Ring::GetToNextRingId();
}

std::vector<Jewelry*> Controller::getAllJewelry() const
{
	return this->repository.get_jewelry_repo();
}

std::vector<Jewelry*> Controller::getAllJewelryMadeOfMaterial(Material material) const
{
	return this->repository.get_by_material(material);
}

std::vector<Jewelry*> Controller::getAllJewelryWithGemstone() const
{
	return this->repository.get_by_gemstone(true);
}

std::vector<Jewelry*> Controller::getAllJewelryWithoutGemstone() const
{
	return this->repository.get_by_gemstone(false);
}

std::vector<Jewelry*> Controller::getAllJewelryWithPriceLessThan(double price) const
{
	return this->repository.get_by_price(isincreasing,price);
}

std::vector<Jewelry*> Controller::getAllJewelryWithPriceGreaterThan(double price) const
{
	return this->repository.get_by_price(isdecreasing, price);
}

std::vector<Jewelry*> Controller::getAllJewelryWithPriceEqualTo(double price) const
{
	return this->repository.get_by_price(isequal, price);
}

std::vector<Jewelry*> Controller::getAllJewelryFromCollection(std::string collection) const
{
	return this->repository.get_by_collection(collection);
}

std::vector<Jewelry*> Controller::getAllNecklaces() const
{
	return this->repository.get_all_necklaces();
}

std::vector<Jewelry*> Controller::getAllNecklacesOfLengthLessThan(float length) const
{
	return this->repository.get_all_necklaces_of_length(isincreasing, length);
}

std::vector<Jewelry*> Controller::getAllNecklacesOfLengthGreaterThan(float length) const
{
	return this->repository.get_all_necklaces_of_length(isdecreasing, length);
}

std::vector<Jewelry*> Controller::getAllNecklacesOfLengthEqualTo(float length) const
{
	return this->repository.get_all_necklaces_of_length(isequal, length);
}

std::vector<Jewelry*> Controller::getAllRings() const
{
	return this->repository.get_all_rings();
}

std::vector<Jewelry*> Controller::getAllRingsOfSizeLessThan(float size) const
{
	return this->repository.get_all_rings_of_size(isincreasing, size);
}

std::vector<Jewelry*> Controller::getAllRingsOfSizeGreaterThan(float size) const
{
	return this->repository.get_all_rings_of_size(isdecreasing, size);
}

std::vector<Jewelry*> Controller::getAllRingsOfSizeEqualTo(float size) const
{
	return this->repository.get_all_rings_of_size(isequal, size);
}

std::vector<Jewelry*> Controller::getAllEarrings() const
{
	return this->repository.get_all_earrings();
}

std::vector<Jewelry*> Controller::getAllEarringsOfType(EaringType earringtype) const
{
	return this->repository.get_all_earrings_of_type(earringtype);
}

std::vector<Jewelry*> Controller::getAllPairsOfEarrings() const
{
	return this->repository.get_all_earrings_piercing_or_not(false);
}

std::vector<Jewelry*> Controller::getAllPiercings() const
{
	return this->repository.get_all_earrings_piercing_or_not(true);
}

std::ostream& operator<<(std::ostream& out, const Controller& controller)
{
	out << controller.repository;
	return out;
}



