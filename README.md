**This project is an inventory management application for a jewelry store. **
The main class is the Jewelry class.
It has three subclasses: Necklace, Earrings and Ring. My application stores only objects of the subclasses of the main class.
All the data is stored in a repository, I created a class for it with basic operations(filtering, adding, removing, updating, undo, redo, etc)
The connection between the User Interface and the Repository is done through a controller, i also created a class for it with operations that call the methoods of the repository class.
I have two types of user interfaces, this is also done through classes:
	- the console user interface is created through the class UI and it is a meniu based aplication. It has basic operations for user imput validation, writing in the console, displaying the result of certain operations;
	- the Graphical User Interface is mainly a class created with and through QT objects. It has basic operations for user imput validation and displaying the result of certain operations;
- Both interfaces allow the user to perfotm certain operations by selecting the corespondin options
All the data from the aplication is read from a csv file at the start of the aplication and stored back in that same csv file at the end of the aplication

The data that is stored must have the following attributes
- for the base class Jewelry: 
	+ material- can be one of:  GOLD, SILVER or PLATINUM;
	+ price;
	+ weight; 
	+ has/ doesn't have a gemstone;
	+ collection;

- for the Earrings subclass the data must also have:
	+ is piercing/ is a pair of earrings;
	+ Earring type - can be one of:  STUDS, HOOPS or DROPS;

- for the Necklace subclass the data must also have:
	+ length - the length of the necklace;

- for the Ring subclass the data must also have:
	+ size- the size of the ring;
